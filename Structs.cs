using System;
using System.Runtime.InteropServices;
using System.Text;

namespace VulkanDotNet
{
	public partial class Vk
	{
		public struct ApplicationInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public readonly string pApplicationName;
			public uint applicationVersion;
			public readonly string pEngineName;
			public uint engineVersion;
			public uint apiVersion;
		};

		public struct InstanceCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public readonly IntPtr pApplicationInfo;
			public uint enabledLayerCount;
			public string ppEnabledLayerNames;
			public uint enabledExtensionCount;
			public string ppEnabledExtensionNames;
		};

		public struct AllocationCallbacks 
		{
			public IntPtr pUserData;
			public PFN_vkAllocationFunction pfnAllocation;
			public PFN_vkReallocationFunction pfnReallocation;
			public PFN_vkFreeFunction pfnFree;
			public PFN_vkInternalAllocationNotification pfnInternalAllocation;
			public PFN_vkInternalFreeNotification pfnInternalFree;
		};

		public struct PhysicalDeviceFeatures 
		{
			public uint robustBufferAccess;
			public uint fullDrawIndexUint32;
			public uint imageCubeArray;
			public uint independentBlend;
			public uint geometryShader;
			public uint tessellationShader;
			public uint sampleRateShading;
			public uint dualSrcBlend;
			public uint logicOp;
			public uint multiDrawIndirect;
			public uint drawIndirectFirstInstance;
			public uint depthClamp;
			public uint depthBiasClamp;
			public uint fillModeNonSolid;
			public uint depthBounds;
			public uint wideLines;
			public uint largePoints;
			public uint alphaToOne;
			public uint multiViewport;
			public uint samplerAnisotropy;
			public uint textureCompressionETC2;
			public uint textureCompressionASTC_LDR;
			public uint textureCompressionBC;
			public uint occlusionQueryPrecise;
			public uint pipelineStatisticsQuery;
			public uint vertexPipelineStoresAndAtomics;
			public uint fragmentStoresAndAtomics;
			public uint shaderTessellationAndGeometryPointSize;
			public uint shaderImageGatherExtended;
			public uint shaderStorageImageExtendedFormats;
			public uint shaderStorageImageMultisample;
			public uint shaderStorageImageReadWithoutFormat;
			public uint shaderStorageImageWriteWithoutFormat;
			public uint shaderUniformBufferArrayDynamicIndexing;
			public uint shaderSampledImageArrayDynamicIndexing;
			public uint shaderStorageBufferArrayDynamicIndexing;
			public uint shaderStorageImageArrayDynamicIndexing;
			public uint shaderClipDistance;
			public uint shaderCullDistance;
			public uint shaderFloat64;
			public uint shaderInt64;
			public uint shaderInt16;
			public uint shaderResourceResidency;
			public uint shaderResourceMinLod;
			public uint sparseBinding;
			public uint sparseResidencyBuffer;
			public uint sparseResidencyImage2D;
			public uint sparseResidencyImage3D;
			public uint sparseResidency2Samples;
			public uint sparseResidency4Samples;
			public uint sparseResidency8Samples;
			public uint sparseResidency16Samples;
			public uint sparseResidencyAliased;
			public uint variableMultisampleRate;
			public uint inheritedQueries;
		};

		public struct FormatProperties 
		{
			public uint linearTilingFeatures;
			public uint optimalTilingFeatures;
			public uint bufferFeatures;
		};

		public struct Extent3D 
		{
			public uint width;
			public uint height;
			public uint depth;
		};

		public struct ImageFormatProperties 
		{
			public Extent3D maxExtent;
			public uint maxMipLevels;
			public uint maxArrayLayers;
			public uint sampleCounts;
			public ulong maxResourceSize;
		};

		public struct PhysicalDeviceLimits 
		{
			public uint maxImageDimension1D;
			public uint maxImageDimension2D;
			public uint maxImageDimension3D;
			public uint maxImageDimensionCube;
			public uint maxImageArrayLayers;
			public uint maxTexelBufferElements;
			public uint maxUniformBufferRange;
			public uint maxStorageBufferRange;
			public uint maxPushConstantsSize;
			public uint maxMemoryAllocationCount;
			public uint maxSamplerAllocationCount;
			public ulong bufferImageGranularity;
			public ulong sparseAddressSpaceSize;
			public uint maxBoundDescriptorSets;
			public uint maxPerStageDescriptorSamplers;
			public uint maxPerStageDescriptorUniformBuffers;
			public uint maxPerStageDescriptorStorageBuffers;
			public uint maxPerStageDescriptorSampledImages;
			public uint maxPerStageDescriptorStorageImages;
			public uint maxPerStageDescriptorInputAttachments;
			public uint maxPerStageResources;
			public uint maxDescriptorSetSamplers;
			public uint maxDescriptorSetUniformBuffers;
			public uint maxDescriptorSetUniformBuffersDynamic;
			public uint maxDescriptorSetStorageBuffers;
			public uint maxDescriptorSetStorageBuffersDynamic;
			public uint maxDescriptorSetSampledImages;
			public uint maxDescriptorSetStorageImages;
			public uint maxDescriptorSetInputAttachments;
			public uint maxVertexInputAttributes;
			public uint maxVertexInputBindings;
			public uint maxVertexInputAttributeOffset;
			public uint maxVertexInputBindingStride;
			public uint maxVertexOutputComponents;
			public uint maxTessellationGenerationLevel;
			public uint maxTessellationPatchSize;
			public uint maxTessellationControlPerVertexInputComponents;
			public uint maxTessellationControlPerVertexOutputComponents;
			public uint maxTessellationControlPerPatchOutputComponents;
			public uint maxTessellationControlTotalOutputComponents;
			public uint maxTessellationEvaluationInputComponents;
			public uint maxTessellationEvaluationOutputComponents;
			public uint maxGeometryShaderInvocations;
			public uint maxGeometryInputComponents;
			public uint maxGeometryOutputComponents;
			public uint maxGeometryOutputVertices;
			public uint maxGeometryTotalOutputComponents;
			public uint maxFragmentInputComponents;
			public uint maxFragmentOutputAttachments;
			public uint maxFragmentDualSrcAttachments;
			public uint maxFragmentCombinedOutputResources;
			public uint maxComputeSharedMemorySize;
			public uint[] maxComputeWorkGroupCount;
			public uint maxComputeWorkGroupInvocations;
			public uint[] maxComputeWorkGroupSize;
			public uint subPixelPrecisionBits;
			public uint subTexelPrecisionBits;
			public uint mipmapPrecisionBits;
			public uint maxDrawIndexedIndexValue;
			public uint maxDrawIndirectCount;
			public float maxSamplerLodBias;
			public float maxSamplerAnisotropy;
			public uint maxViewports;
			public uint[] maxViewportDimensions;
			public float[] viewportBoundsRange;
			public uint viewportSubPixelBits;
			public ulong minMemoryMapAlignment;
			public ulong minTexelBufferOffsetAlignment;
			public ulong minUniformBufferOffsetAlignment;
			public ulong minStorageBufferOffsetAlignment;
			public int minTexelOffset;
			public uint maxTexelOffset;
			public int minTexelGatherOffset;
			public uint maxTexelGatherOffset;
			public float minInterpolationOffset;
			public float maxInterpolationOffset;
			public uint subPixelInterpolationOffsetBits;
			public uint maxFramebufferWidth;
			public uint maxFramebufferHeight;
			public uint maxFramebufferLayers;
			public uint framebufferColorSampleCounts;
			public uint framebufferDepthSampleCounts;
			public uint framebufferStencilSampleCounts;
			public uint framebufferNoAttachmentsSampleCounts;
			public uint maxColorAttachments;
			public uint sampledImageColorSampleCounts;
			public uint sampledImageIntegerSampleCounts;
			public uint sampledImageDepthSampleCounts;
			public uint sampledImageStencilSampleCounts;
			public uint storageImageSampleCounts;
			public uint maxSampleMaskWords;
			public uint timestampComputeAndGraphics;
			public float timestampPeriod;
			public uint maxClipDistances;
			public uint maxCullDistances;
			public uint maxCombinedClipAndCullDistances;
			public uint discreteQueuePriorities;
			public float[] pointSizeRange;
			public float[] lineWidthRange;
			public float pointSizeGranularity;
			public float lineWidthGranularity;
			public uint strictLines;
			public uint standardSampleLocations;
			public ulong optimalBufferCopyOffsetAlignment;
			public ulong optimalBufferCopyRowPitchAlignment;
			public ulong nonCoherentAtomSize;
		};

		public struct PhysicalDeviceSparseProperties 
		{
			public uint residencyStandard2DBlockShape;
			public uint residencyStandard2DMultisampleBlockShape;
			public uint residencyStandard3DBlockShape;
			public uint residencyAlignedMipSize;
			public uint residencyNonResidentStrict;
		};

		public struct PhysicalDeviceProperties 
		{
			public uint apiVersion;
			public uint driverVersion;
			public uint vendorID;
			public uint deviceID;
			public PhysicalDeviceType deviceType;
			public char[] deviceName;
			public byte[] pipelineCacheUUID;
			public PhysicalDeviceLimits limits;
			public PhysicalDeviceSparseProperties sparseProperties;
		};

		public struct QueueFamilyProperties 
		{
			public uint queueFlags;
			public uint queueCount;
			public uint timestampValidBits;
			public Extent3D minImageTransferGranularity;
		};

		public struct MemoryType 
		{
			public uint propertyFlags;
			public uint heapIndex;
		};

		public struct MemoryHeap 
		{
			public ulong size;
			public uint flags;
		};

		public struct PhysicalDeviceMemoryProperties 
		{
			public uint memoryTypeCount;
			public MemoryType[] memoryTypes;
			public uint memoryHeapCount;
			public MemoryHeap[] memoryHeaps;
		};

		public struct DeviceQueueCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint queueFamilyIndex;
			public uint queueCount;
			public readonly IntPtr pQueuePriorities;
		};

		public struct DeviceCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint queueCreateInfoCount;
			public readonly IntPtr pQueueCreateInfos;
			public uint enabledLayerCount;
			public string ppEnabledLayerNames;
			public uint enabledExtensionCount;
			public string ppEnabledExtensionNames;
			public readonly IntPtr pEnabledFeatures;
		};

		public struct ExtensionProperties 
		{
			public char[] extensionName;
			public uint specVersion;
		};

		public struct LayerProperties 
		{
			public char[] layerName;
			public uint specVersion;
			public uint implementationVersion;
			public char[] description;
		};

		public struct SubmitInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint waitSemaphoreCount;
			public readonly IntPtr pWaitSemaphores;
			public readonly IntPtr pWaitDstStageMask;
			public uint commandBufferCount;
			public readonly IntPtr pCommandBuffers;
			public uint signalSemaphoreCount;
			public readonly IntPtr pSignalSemaphores;
		};

		public struct MemoryAllocateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public ulong allocationSize;
			public uint memoryTypeIndex;
		};

		public struct MappedMemoryRange 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr memory;
			public ulong offset;
			public ulong size;
		};

		public struct MemoryRequirements 
		{
			public ulong size;
			public ulong alignment;
			public uint memoryTypeBits;
		};

		public struct SparseImageFormatProperties 
		{
			public uint aspectMask;
			public Extent3D imageGranularity;
			public uint flags;
		};

		public struct SparseImageMemoryRequirements 
		{
			public SparseImageFormatProperties formatProperties;
			public uint imageMipTailFirstLod;
			public ulong imageMipTailSize;
			public ulong imageMipTailOffset;
			public ulong imageMipTailStride;
		};

		public struct SparseMemoryBind 
		{
			public ulong resourceOffset;
			public ulong size;
			public IntPtr memory;
			public ulong memoryOffset;
			public uint flags;
		};

		public struct SparseBufferMemoryBindInfo 
		{
			public IntPtr buffer;
			public uint bindCount;
			public readonly IntPtr pBinds;
		};

		public struct SparseImageOpaqueMemoryBindInfo 
		{
			public IntPtr image;
			public uint bindCount;
			public readonly IntPtr pBinds;
		};

		public struct ImageSubresource 
		{
			public uint aspectMask;
			public uint mipLevel;
			public uint arrayLayer;
		};

		public struct Offset3D 
		{
			public int x;
			public int y;
			public int z;
		};

		public struct SparseImageMemoryBind 
		{
			public ImageSubresource subresource;
			public Offset3D offset;
			public Extent3D extent;
			public IntPtr memory;
			public ulong memoryOffset;
			public uint flags;
		};

		public struct SparseImageMemoryBindInfo 
		{
			public IntPtr image;
			public uint bindCount;
			public readonly IntPtr pBinds;
		};

		public struct BindSparseInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint waitSemaphoreCount;
			public readonly IntPtr pWaitSemaphores;
			public uint bufferBindCount;
			public readonly IntPtr pBufferBinds;
			public uint imageOpaqueBindCount;
			public readonly IntPtr pImageOpaqueBinds;
			public uint imageBindCount;
			public readonly IntPtr pImageBinds;
			public uint signalSemaphoreCount;
			public readonly IntPtr pSignalSemaphores;
		};

		public struct FenceCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
		};

		public struct SemaphoreCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
		};

		public struct EventCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
		};

		public struct QueryPoolCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public QueryType queryType;
			public uint queryCount;
			public uint pipelineStatistics;
		};

		public struct BufferCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public ulong size;
			public uint usage;
			public SharingMode sharingMode;
			public uint queueFamilyIndexCount;
			public readonly IntPtr pQueueFamilyIndices;
		};

		public struct BufferViewCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr buffer;
			public Format format;
			public ulong offset;
			public ulong range;
		};

		public struct ImageCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public ImageType imageType;
			public Format format;
			public Extent3D extent;
			public uint mipLevels;
			public uint arrayLayers;
			public SampleCountFlagBits samples;
			public ImageTiling tiling;
			public uint usage;
			public SharingMode sharingMode;
			public uint queueFamilyIndexCount;
			public readonly IntPtr pQueueFamilyIndices;
			public ImageLayout initialLayout;
		};

		public struct SubresourceLayout 
		{
			public ulong offset;
			public ulong size;
			public ulong rowPitch;
			public ulong arrayPitch;
			public ulong depthPitch;
		};

		public struct ComponentMapping 
		{
			public ComponentSwizzle r;
			public ComponentSwizzle g;
			public ComponentSwizzle b;
			public ComponentSwizzle a;
		};

		public struct ImageSubresourceRange 
		{
			public uint aspectMask;
			public uint baseMipLevel;
			public uint levelCount;
			public uint baseArrayLayer;
			public uint layerCount;
		};

		public struct ImageViewCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr image;
			public ImageViewType viewType;
			public Format format;
			public ComponentMapping components;
			public ImageSubresourceRange subresourceRange;
		};

		public struct ShaderModuleCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public ulong codeSize;
			public readonly IntPtr pCode;
		};

		public struct PipelineCacheCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public ulong initialDataSize;
			public readonly IntPtr pInitialData;
		};

		public struct SpecializationMapEntry 
		{
			public uint readonlyantID;
			public uint offset;
			public ulong size;
		};

		public struct SpecializationInfo 
		{
			public uint mapEntryCount;
			public readonly IntPtr pMapEntries;
			public ulong dataSize;
			public readonly IntPtr pData;
		};

		public struct PipelineShaderStageCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public ShaderStageFlagBits stage;
			public IntPtr module;
			public readonly string pName;
			public readonly IntPtr pSpecializationInfo;
		};

		public struct VertexInputBindingDescription 
		{
			public uint binding;
			public uint stride;
			public VertexInputRate inputRate;
		};

		public struct VertexInputAttributeDescription 
		{
			public uint location;
			public uint binding;
			public Format format;
			public uint offset;
		};

		public struct PipelineVertexInputStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint vertexBindingDescriptionCount;
			public readonly IntPtr pVertexBindingDescriptions;
			public uint vertexAttributeDescriptionCount;
			public readonly IntPtr pVertexAttributeDescriptions;
		};

		public struct PipelineInputAssemblyStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public PrimitiveTopology topology;
			public uint primitiveRestartEnable;
		};

		public struct PipelineTessellationStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint patchControlPoints;
		};

		public struct Viewport 
		{
			public float x;
			public float y;
			public float width;
			public float height;
			public float minDepth;
			public float maxDepth;
		};

		public struct Offset2D 
		{
			public int x;
			public int y;
		};

		public struct Extent2D 
		{
			public uint width;
			public uint height;
		};

		public struct Rect2D 
		{
			public Offset2D offset;
			public Extent2D extent;
		};

		public struct PipelineViewportStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint viewportCount;
			public readonly IntPtr pViewports;
			public uint scissorCount;
			public readonly IntPtr pScissors;
		};

		public struct PipelineRasterizationStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint depthClampEnable;
			public uint rasterizerDiscardEnable;
			public PolygonMode polygonMode;
			public uint cullMode;
			public FrontFace frontFace;
			public uint depthBiasEnable;
			public float depthBiasConstantFactor;
			public float depthBiasClamp;
			public float depthBiasSlopeFactor;
			public float lineWidth;
		};

		public struct PipelineMultisampleStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public SampleCountFlagBits rasterizationSamples;
			public uint sampleShadingEnable;
			public float minSampleShading;
			public readonly IntPtr pSampleMask;
			public uint alphaToCoverageEnable;
			public uint alphaToOneEnable;
		};

		public struct StencilOpState 
		{
			public StencilOp failOp;
			public StencilOp passOp;
			public StencilOp depthFailOp;
			public CompareOp compareOp;
			public uint compareMask;
			public uint writeMask;
			public uint reference;
		};

		public struct PipelineDepthStencilStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint depthTestEnable;
			public uint depthWriteEnable;
			public CompareOp depthCompareOp;
			public uint depthBoundsTestEnable;
			public uint stencilTestEnable;
			public StencilOpState front;
			public StencilOpState back;
			public float minDepthBounds;
			public float maxDepthBounds;
		};

		public struct PipelineColorBlendAttachmentState 
		{
			public uint blendEnable;
			public BlendFactor srcColorBlendFactor;
			public BlendFactor dstColorBlendFactor;
			public BlendOp colorBlendOp;
			public BlendFactor srcAlphaBlendFactor;
			public BlendFactor dstAlphaBlendFactor;
			public BlendOp alphaBlendOp;
			public uint colorWriteMask;
		};

		public struct PipelineColorBlendStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint logicOpEnable;
			public LogicOp logicOp;
			public uint attachmentCount;
			public readonly IntPtr pAttachments;
			public float[] blendConstants;
		};

		public struct PipelineDynamicStateCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint dynamicStateCount;
			public readonly IntPtr pDynamicStates;
		};

		public struct GraphicsPipelineCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint stageCount;
			public readonly IntPtr pStages;
			public readonly IntPtr pVertexInputState;
			public readonly IntPtr pInputAssemblyState;
			public readonly IntPtr pTessellationState;
			public readonly IntPtr pViewportState;
			public readonly IntPtr pRasterizationState;
			public readonly IntPtr pMultisampleState;
			public readonly IntPtr pDepthStencilState;
			public readonly IntPtr pColorBlendState;
			public readonly IntPtr pDynamicState;
			public IntPtr layout;
			public IntPtr renderPass;
			public uint subpass;
			public IntPtr basePipelineHandle;
			public int basePipelineIndex;
		};

		public struct ComputePipelineCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public PipelineShaderStageCreateInfo stage;
			public IntPtr layout;
			public IntPtr basePipelineHandle;
			public int basePipelineIndex;
		};

		public struct PushConstantRange 
		{
			public uint stageFlags;
			public uint offset;
			public uint size;
		};

		public struct PipelineLayoutCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint setLayoutCount;
			public readonly IntPtr pSetLayouts;
			public uint pushConstantRangeCount;
			public readonly IntPtr pPushConstantRanges;
		};

		public struct SamplerCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public Filter magFilter;
			public Filter minFilter;
			public SamplerMipmapMode mipmapMode;
			public SamplerAddressMode addressModeU;
			public SamplerAddressMode addressModeV;
			public SamplerAddressMode addressModeW;
			public float mipLodBias;
			public uint anisotropyEnable;
			public float maxAnisotropy;
			public uint compareEnable;
			public CompareOp compareOp;
			public float minLod;
			public float maxLod;
			public BorderColor borderColor;
			public uint unnormalizedCoordinates;
		};

		public struct DescriptorSetLayoutBinding 
		{
			public uint binding;
			public DescriptorType descriptorType;
			public uint descriptorCount;
			public uint stageFlags;
			public readonly IntPtr pImmutableSamplers;
		};

		public struct DescriptorSetLayoutCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint bindingCount;
			public readonly IntPtr pBindings;
		};

		public struct DescriptorPoolSize 
		{
			public DescriptorType type;
			public uint descriptorCount;
		};

		public struct DescriptorPoolCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint maxSets;
			public uint poolSizeCount;
			public readonly IntPtr pPoolSizes;
		};

		public struct DescriptorSetAllocateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr descriptorPool;
			public uint descriptorSetCount;
			public readonly IntPtr pSetLayouts;
		};

		public struct DescriptorImageInfo 
		{
			public IntPtr sampler;
			public IntPtr imageView;
			public ImageLayout imageLayout;
		};

		public struct DescriptorBufferInfo 
		{
			public IntPtr buffer;
			public ulong offset;
			public ulong range;
		};

		public struct WriteDescriptorSet 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr dstSet;
			public uint dstBinding;
			public uint dstArrayElement;
			public uint descriptorCount;
			public DescriptorType descriptorType;
			public readonly IntPtr pImageInfo;
			public readonly IntPtr pBufferInfo;
			public readonly IntPtr pTexelBufferView;
		};

		public struct CopyDescriptorSet 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr srcSet;
			public uint srcBinding;
			public uint srcArrayElement;
			public IntPtr dstSet;
			public uint dstBinding;
			public uint dstArrayElement;
			public uint descriptorCount;
		};

		public struct FramebufferCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr renderPass;
			public uint attachmentCount;
			public readonly IntPtr pAttachments;
			public uint width;
			public uint height;
			public uint layers;
		};

		public struct AttachmentDescription 
		{
			public uint flags;
			public Format format;
			public SampleCountFlagBits samples;
			public AttachmentLoadOp loadOp;
			public AttachmentStoreOp storeOp;
			public AttachmentLoadOp stencilLoadOp;
			public AttachmentStoreOp stencilStoreOp;
			public ImageLayout initialLayout;
			public ImageLayout finalLayout;
		};

		public struct AttachmentReference 
		{
			public uint attachment;
			public ImageLayout layout;
		};

		public struct SubpassDescription 
		{
			public uint flags;
			public PipelineBindPoint pipelineBindPoint;
			public uint inputAttachmentCount;
			public readonly IntPtr pInputAttachments;
			public uint colorAttachmentCount;
			public readonly IntPtr pColorAttachments;
			public readonly IntPtr pResolveAttachments;
			public readonly IntPtr pDepthStencilAttachment;
			public uint preserveAttachmentCount;
			public readonly IntPtr pPreserveAttachments;
		};

		public struct SubpassDependency 
		{
			public uint srcSubpass;
			public uint dstSubpass;
			public uint srcStageMask;
			public uint dstStageMask;
			public uint srcAccessMask;
			public uint dstAccessMask;
			public uint dependencyFlags;
		};

		public struct RenderPassCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint attachmentCount;
			public readonly IntPtr pAttachments;
			public uint subpassCount;
			public readonly IntPtr pSubpasses;
			public uint dependencyCount;
			public readonly IntPtr pDependencies;
		};

		public struct CommandPoolCreateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public uint queueFamilyIndex;
		};

		public struct CommandBufferAllocateInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr commandPool;
			public CommandBufferLevel level;
			public uint commandBufferCount;
		};

		public struct CommandBufferInheritanceInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr renderPass;
			public uint subpass;
			public IntPtr framebuffer;
			public uint occlusionQueryEnable;
			public uint queryFlags;
			public uint pipelineStatistics;
		};

		public struct CommandBufferBeginInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public readonly IntPtr pInheritanceInfo;
		};

		public struct BufferCopy 
		{
			public ulong srcOffset;
			public ulong dstOffset;
			public ulong size;
		};

		public struct ImageSubresourceLayers 
		{
			public uint aspectMask;
			public uint mipLevel;
			public uint baseArrayLayer;
			public uint layerCount;
		};

		public struct ImageCopy 
		{
			public ImageSubresourceLayers srcSubresource;
			public Offset3D srcOffset;
			public ImageSubresourceLayers dstSubresource;
			public Offset3D dstOffset;
			public Extent3D extent;
		};

		public struct ImageBlit 
		{
			public ImageSubresourceLayers srcSubresource;
			public Offset3D[] srcOffsets;
			public ImageSubresourceLayers dstSubresource;
			public Offset3D[] dstOffsets;
		};

		public struct BufferImageCopy 
		{
			public ulong bufferOffset;
			public uint bufferRowLength;
			public uint bufferImageHeight;
			public ImageSubresourceLayers imageSubresource;
			public Offset3D imageOffset;
			public Extent3D imageExtent;
		};

		[StructLayout(LayoutKind.Explicit)]
		public struct ClearColorValue 
		{
			[FieldOffset(0)]
			public float[] float32;
			[FieldOffset(0)]
			public int[] int32;
			[FieldOffset(0)]
			public uint[] uint32;
		};

		public struct ClearDepthStencilValue 
		{
			public float depth;
			public uint stencil;
		};

		[StructLayout(LayoutKind.Explicit)]
		public struct ClearValue 
		{
			[FieldOffset(0)]
			public ClearColorValue color;
			[FieldOffset(0)]
			public ClearDepthStencilValue depthStencil;
		};

		public struct ClearAttachment 
		{
			public uint aspectMask;
			public uint colorAttachment;
			public ClearValue clearValue;
		};

		public struct ClearRect 
		{
			public Rect2D rect;
			public uint baseArrayLayer;
			public uint layerCount;
		};

		public struct ImageResolve 
		{
			public ImageSubresourceLayers srcSubresource;
			public Offset3D srcOffset;
			public ImageSubresourceLayers dstSubresource;
			public Offset3D dstOffset;
			public Extent3D extent;
		};

		public struct MemoryBarrier 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint srcAccessMask;
			public uint dstAccessMask;
		};

		public struct BufferMemoryBarrier 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint srcAccessMask;
			public uint dstAccessMask;
			public uint srcQueueFamilyIndex;
			public uint dstQueueFamilyIndex;
			public IntPtr buffer;
			public ulong offset;
			public ulong size;
		};

		public struct ImageMemoryBarrier 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint srcAccessMask;
			public uint dstAccessMask;
			public ImageLayout oldLayout;
			public ImageLayout newLayout;
			public uint srcQueueFamilyIndex;
			public uint dstQueueFamilyIndex;
			public IntPtr image;
			public ImageSubresourceRange subresourceRange;
		};

		public struct RenderPassBeginInfo 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr renderPass;
			public IntPtr framebuffer;
			public Rect2D renderArea;
			public uint clearValueCount;
			public readonly IntPtr pClearValues;
		};

		public struct DispatchIndirectCommand 
		{
			public uint x;
			public uint y;
			public uint z;
		};

		public struct DrawIndexedIndirectCommand 
		{
			public uint indexCount;
			public uint instanceCount;
			public uint firstIndex;
			public int vertexOffset;
			public uint firstInstance;
		};

		public struct DrawIndirectCommand 
		{
			public uint vertexCount;
			public uint instanceCount;
			public uint firstVertex;
			public uint firstInstance;
		};

		public struct SurfaceCapabilitiesKHR 
		{
			public uint minImageCount;
			public uint maxImageCount;
			public Extent2D currentExtent;
			public Extent2D minImageExtent;
			public Extent2D maxImageExtent;
			public uint maxImageArrayLayers;
			public uint supportedTransforms;
			public SurfaceTransformFlagBitsKHR currentTransform;
			public uint supportedCompositeAlpha;
			public uint supportedUsageFlags;
		};

		public struct SurfaceFormatKHR 
		{
			public Format format;
			public ColorSpaceKHR colorSpace;
		};

		public struct SwapchainCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr surface;
			public uint minImageCount;
			public Format imageFormat;
			public ColorSpaceKHR imageColorSpace;
			public Extent2D imageExtent;
			public uint imageArrayLayers;
			public uint imageUsage;
			public SharingMode imageSharingMode;
			public uint queueFamilyIndexCount;
			public readonly IntPtr pQueueFamilyIndices;
			public SurfaceTransformFlagBitsKHR preTransform;
			public CompositeAlphaFlagBitsKHR compositeAlpha;
			public PresentModeKHR presentMode;
			public uint clipped;
			public IntPtr oldSwapchain;
		};

		public struct PresentInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint waitSemaphoreCount;
			public readonly IntPtr pWaitSemaphores;
			public uint swapchainCount;
			public readonly IntPtr pSwapchains;
			public readonly IntPtr pImageIndices;
			public IntPtr pResults;
		};

		public struct DisplayPropertiesKHR 
		{
			public IntPtr display;
			public readonly string displayName;
			public Extent2D physicalDimensions;
			public Extent2D physicalResolution;
			public uint supportedTransforms;
			public uint planeReorderPossible;
			public uint persistentContent;
		};

		public struct DisplayModeParametersKHR 
		{
			public Extent2D visibleRegion;
			public uint refreshRate;
		};

		public struct DisplayModePropertiesKHR 
		{
			public IntPtr displayMode;
			public DisplayModeParametersKHR parameters;
		};

		public struct DisplayModeCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public DisplayModeParametersKHR parameters;
		};

		public struct DisplayPlaneCapabilitiesKHR 
		{
			public uint supportedAlpha;
			public Offset2D minSrcPosition;
			public Offset2D maxSrcPosition;
			public Extent2D minSrcExtent;
			public Extent2D maxSrcExtent;
			public Offset2D minDstPosition;
			public Offset2D maxDstPosition;
			public Extent2D minDstExtent;
			public Extent2D maxDstExtent;
		};

		public struct DisplayPlanePropertiesKHR 
		{
			public IntPtr currentDisplay;
			public uint currentStackIndex;
		};

		public struct DisplaySurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr displayMode;
			public uint planeIndex;
			public uint planeStackIndex;
			public SurfaceTransformFlagBitsKHR transform;
			public float globalAlpha;
			public DisplayPlaneAlphaFlagBitsKHR alphaMode;
			public Extent2D imageExtent;
		};

		public struct DisplayPresentInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public Rect2D srcRect;
			public Rect2D dstRect;
			public uint persistent;
		};

		public struct XlibSurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr dpy;
			public ulong window;
		};

		public struct XcbSurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr connection;
			public UInt32 window;
		};

		public struct WaylandSurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr display;
			public IntPtr surface;
		};

		public struct MirSurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr connection;
			public IntPtr mirSurface;
		};

		public struct AndroidSurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr window;
		};

		public struct Win32SurfaceCreateInfoKHR 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public IntPtr hinstance;
			public IntPtr hwnd;
		};

		public struct DebugReportCallbackCreateInfoEXT 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint flags;
			public PFN_vkDebugReportCallbackEXT pfnCallback;
			public IntPtr pUserData;
		};

		public struct PipelineRasterizationStateRasterizationOrderAMD 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public RasterizationOrderAMD rasterizationOrder;
		};

		public struct DebugMarkerObjectNameInfoEXT 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public DebugReportObjectTypeEXT objectType;
			public ulong obj;
			public readonly string pObjectName;
		};

		public struct DebugMarkerObjectTagInfoEXT 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public DebugReportObjectTypeEXT objectType;
			public ulong obj;
			public ulong tagName;
			public ulong tagSize;
			public readonly IntPtr pTag;
		};

		public struct DebugMarkerMarkerInfoEXT 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public readonly string pMarkerName;
			public float[] color;
		};

		public struct DedicatedAllocationImageCreateInfoNV 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint dedicatedAllocation;
		};

		public struct DedicatedAllocationBufferCreateInfoNV 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public uint dedicatedAllocation;
		};

		public struct DedicatedAllocationMemoryAllocateInfoNV 
		{
			public StructureType sType;
			public readonly IntPtr pNext;
			public IntPtr image;
			public IntPtr buffer;
		};

	}
}