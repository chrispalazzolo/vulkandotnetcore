using System;
using System.Runtime.InteropServices;
using System.Text;

namespace VulkanDotNet
{
	public partial class Vk
	{
#if WINDOWS
		private const string VK_OS_LIB_FILE = "vulkan-1.dll";
#elif OSX
		private const string VK_OS_LIB_FILE = "libvulkan.so";
#elif LINUX
		private const string VK_OS_LIB_FILE = "libvulkan.so";
#endif

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateInstance", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateInstance(IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pInstance);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyInstance", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyInstance(IntPtr instance, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEnumeratePhysicalDevices", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEnumeratePhysicalDevices(IntPtr instance, ref uint pPhysicalDeviceCount, IntPtr pPhysicalDevices);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceFeatures", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceFeatures(IntPtr physicalDevice, IntPtr pFeatures);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceFormatProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceFormatProperties(IntPtr physicalDevice, Format format, IntPtr pFormatProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceImageFormatProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, ImageTiling tiling, uint usage, uint flags, IntPtr pImageFormatProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceProperties(IntPtr physicalDevice, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceQueueFamilyProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceQueueFamilyProperties(IntPtr physicalDevice, ref uint pQueueFamilyPropertyCount, IntPtr pQueueFamilyProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceMemoryProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceMemoryProperties(IntPtr physicalDevice, IntPtr pMemoryProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetInstanceProcAddr", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern PFNvkVoidFunction vkGetInstanceProcAddr(IntPtr instance, string pName);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDeviceProcAddr", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern PFNvkVoidFunction vkGetDeviceProcAddr(IntPtr device, string pName);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDevice", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDevice(IntPtr physicalDevice, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDevice);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyDevice", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyDevice(IntPtr device, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEnumerateInstanceExtensionProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEnumerateInstanceExtensionProperties(string pLayerName, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEnumerateDeviceExtensionProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEnumerateDeviceExtensionProperties(IntPtr physicalDevice, string pLayerName, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEnumerateInstanceLayerProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEnumerateInstanceLayerProperties(ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEnumerateDeviceLayerProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEnumerateDeviceLayerProperties(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDeviceQueue", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetDeviceQueue(IntPtr device, uint queueFamilyIndex, uint queueIndex, IntPtr pQueue);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkQueueSubmit", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkQueueSubmit(IntPtr queue, uint submitCount, IntPtr pSubmits, IntPtr fence);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkQueueWaitIdle", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkQueueWaitIdle(IntPtr queue);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDeviceWaitIdle", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkDeviceWaitIdle(IntPtr device);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkAllocateMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkAllocateMemory(IntPtr device, IntPtr pAllocateInfo, IntPtr pAllocator, IntPtr pMemory);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkFreeMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkFreeMemory(IntPtr device, IntPtr memory, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkMapMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkMapMemory(IntPtr device, IntPtr memory, ulong offset, ulong size, uint flags, IntPtr ppData);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkUnmapMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkUnmapMemory(IntPtr device, IntPtr memory);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkFlushMappedMemoryRanges", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkFlushMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkInvalidateMappedMemoryRanges", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkInvalidateMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDeviceMemoryCommitment", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetDeviceMemoryCommitment(IntPtr device, IntPtr memory, ref ulong pCommittedMemoryInBytes);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkBindBufferMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkBindBufferMemory(IntPtr device, IntPtr buffer, IntPtr memory, ulong memoryOffset);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkBindImageMemory", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkBindImageMemory(IntPtr device, IntPtr image, IntPtr memory, ulong memoryOffset);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetBufferMemoryRequirements", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetBufferMemoryRequirements(IntPtr device, IntPtr buffer, IntPtr pMemoryRequirements);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetImageMemoryRequirements", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetImageMemoryRequirements(IntPtr device, IntPtr image, IntPtr pMemoryRequirements);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetImageSparseMemoryRequirements", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetImageSparseMemoryRequirements(IntPtr device, IntPtr image, ref uint pSparseMemoryRequirementCount, IntPtr pSparseMemoryRequirements);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceSparseImageFormatProperties", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetPhysicalDeviceSparseImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, SampleCountFlagBits samples, uint usage, ImageTiling tiling, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkQueueBindSparse", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkQueueBindSparse(IntPtr queue, uint bindInfoCount, IntPtr pBindInfo, IntPtr fence);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateFence", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateFence(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFence);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyFence", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyFence(IntPtr device, IntPtr fence, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkResetFences", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkResetFences(IntPtr device, uint fenceCount, IntPtr pFences);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetFenceStatus", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetFenceStatus(IntPtr device, IntPtr fence);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkWaitForFences", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkWaitForFences(IntPtr device, uint fenceCount, IntPtr pFences, uint waitAll, ulong timeout);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateSemaphore", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateSemaphore(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSemaphore);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroySemaphore", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroySemaphore(IntPtr device, IntPtr semaphore, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateEvent(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pEvent);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyEvent(IntPtr device, IntPtr evnt, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetEventStatus", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetEventStatus(IntPtr device, IntPtr evnt);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkSetEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkSetEvent(IntPtr device, IntPtr evnt);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkResetEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkResetEvent(IntPtr device, IntPtr evnt);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateQueryPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateQueryPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pQueryPool);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyQueryPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyQueryPool(IntPtr device, IntPtr queryPool, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetQueryPoolResults", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetQueryPoolResults(IntPtr device, IntPtr queryPool, uint firstQuery, uint queryCount, ulong dataSize, IntPtr pData, ulong stride, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateBuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pBuffer);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyBuffer(IntPtr device, IntPtr buffer, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateBufferView", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateBufferView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyBufferView", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyBufferView(IntPtr device, IntPtr bufferView, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateImage(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pImage);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyImage(IntPtr device, IntPtr image, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetImageSubresourceLayout", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetImageSubresourceLayout(IntPtr device, IntPtr image, IntPtr pSubresource, IntPtr pLayout);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateImageView", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateImageView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyImageView", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyImageView(IntPtr device, IntPtr imageView, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateShaderModule", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateShaderModule(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pShaderModule);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyShaderModule", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyShaderModule(IntPtr device, IntPtr shaderModule, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreatePipelineCache", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreatePipelineCache(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineCache);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyPipelineCache", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyPipelineCache(IntPtr device, IntPtr pipelineCache, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPipelineCacheData", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPipelineCacheData(IntPtr device, IntPtr pipelineCache, ref ulong pDataSize, IntPtr pData);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkMergePipelineCaches", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkMergePipelineCaches(IntPtr device, IntPtr dstCache, uint srcCacheCount, IntPtr pSrcCaches);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateGraphicsPipelines", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateGraphicsPipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateComputePipelines", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateComputePipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyPipeline", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyPipeline(IntPtr device, IntPtr pipeline, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreatePipelineLayout", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreatePipelineLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineLayout);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyPipelineLayout", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyPipelineLayout(IntPtr device, IntPtr pipelineLayout, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateSampler", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateSampler(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSampler);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroySampler", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroySampler(IntPtr device, IntPtr sampler, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDescriptorSetLayout", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDescriptorSetLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSetLayout);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyDescriptorSetLayout", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyDescriptorSetLayout(IntPtr device, IntPtr descriptorSetLayout, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDescriptorPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDescriptorPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDescriptorPool);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyDescriptorPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyDescriptorPool(IntPtr device, IntPtr descriptorPool, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkResetDescriptorPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkResetDescriptorPool(IntPtr device, IntPtr descriptorPool, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkAllocateDescriptorSets", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkAllocateDescriptorSets(IntPtr device, IntPtr pAllocateInfo, IntPtr pDescriptorSets);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkFreeDescriptorSets", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkFreeDescriptorSets(IntPtr device, IntPtr descriptorPool, uint descriptorSetCount, IntPtr pDescriptorSets);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkUpdateDescriptorSets", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkUpdateDescriptorSets(IntPtr device, uint descriptorWriteCount, IntPtr pDescriptorWrites, uint descriptorCopyCount, IntPtr pDescriptorCopies);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateFramebuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateFramebuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFramebuffer);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyFramebuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyFramebuffer(IntPtr device, IntPtr framebuffer, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateRenderPass", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateRenderPass(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pRenderPass);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyRenderPass", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyRenderPass(IntPtr device, IntPtr renderPass, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetRenderAreaGranularity", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkGetRenderAreaGranularity(IntPtr device, IntPtr renderPass, IntPtr pGranularity);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateCommandPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateCommandPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCommandPool);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyCommandPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyCommandPool(IntPtr device, IntPtr commandPool, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkResetCommandPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkResetCommandPool(IntPtr device, IntPtr commandPool, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkAllocateCommandBuffers", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkAllocateCommandBuffers(IntPtr device, IntPtr pAllocateInfo, IntPtr pCommandBuffers);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkFreeCommandBuffers", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkFreeCommandBuffers(IntPtr device, IntPtr commandPool, uint commandBufferCount, IntPtr pCommandBuffers);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkBeginCommandBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkBeginCommandBuffer(IntPtr commandBuffer, IntPtr pBeginInfo);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkEndCommandBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkEndCommandBuffer(IntPtr commandBuffer);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkResetCommandBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkResetCommandBuffer(IntPtr commandBuffer, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBindPipeline", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBindPipeline(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr pipeline);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetViewport", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetViewport(IntPtr commandBuffer, uint firstViewport, uint viewportCount, IntPtr pViewports);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetScissor", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetScissor(IntPtr commandBuffer, uint firstScissor, uint scissorCount, IntPtr pScissors);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetLineWidth", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetLineWidth(IntPtr commandBuffer, float lineWidth);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetDepthBias", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetDepthBias(IntPtr commandBuffer, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetBlendConstants", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetBlendConstants(IntPtr commandBuffer, float[] blendConstants);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetDepthBounds", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetDepthBounds(IntPtr commandBuffer, float minDepthBounds, float maxDepthBounds);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetStencilCompareMask", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetStencilCompareMask(IntPtr commandBuffer, uint faceMask, uint compareMask);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetStencilWriteMask", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetStencilWriteMask(IntPtr commandBuffer, uint faceMask, uint writeMask);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetStencilReference", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetStencilReference(IntPtr commandBuffer, uint faceMask, uint reference);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBindDescriptorSets", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBindDescriptorSets(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr layout, uint firstSet, uint descriptorSetCount, IntPtr pDescriptorSets, uint dynamicOffsetCount, ref uint pDynamicOffsets);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBindIndexBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBindIndexBuffer(IntPtr commandBuffer, IntPtr buffer, ulong offset, IndexType indexType);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBindVertexBuffers", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBindVertexBuffers(IntPtr commandBuffer, uint firstBinding, uint bindingCount, IntPtr pBuffers, ref ulong pOffsets);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDraw", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDraw(IntPtr commandBuffer, uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDrawIndexed", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDrawIndexed(IntPtr commandBuffer, uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDrawIndirect", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDrawIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDrawIndexedIndirect", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDrawIndexedIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDispatch", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDispatch(IntPtr commandBuffer, uint x, uint y, uint z);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDispatchIndirect", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDispatchIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdCopyBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdCopyBuffer(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstBuffer, uint regionCount, IntPtr pRegions);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdCopyImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdCopyImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBlitImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBlitImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions, Filter filter);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdCopyBufferToImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdCopyBufferToImage(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdCopyImageToBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdCopyImageToBuffer(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstBuffer, uint regionCount, IntPtr pRegions);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdUpdateBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdUpdateBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong dataSize, IntPtr pData);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdFillBuffer", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdFillBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong size, uint data);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdClearColorImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdClearColorImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pColor, uint rangeCount, IntPtr pRanges);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdClearDepthStencilImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdClearDepthStencilImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pDepthStencil, uint rangeCount, IntPtr pRanges);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdClearAttachments", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdClearAttachments(IntPtr commandBuffer, uint attachmentCount, IntPtr pAttachments, uint rectCount, IntPtr pRects);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdResolveImage", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdResolveImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdSetEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdSetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdResetEvent", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdResetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdWaitEvents", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdWaitEvents(IntPtr commandBuffer, uint eventCount, IntPtr pEvents, uint srcStageMask, uint dstStageMask, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdPipelineBarrier", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdPipelineBarrier(IntPtr commandBuffer, uint srcStageMask, uint dstStageMask, uint dependencyFlags, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBeginQuery", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBeginQuery(IntPtr commandBuffer, IntPtr queryPool, uint query, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdEndQuery", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdEndQuery(IntPtr commandBuffer, IntPtr queryPool, uint query);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdResetQueryPool", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdResetQueryPool(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdWriteTimestamp", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdWriteTimestamp(IntPtr commandBuffer, PipelineStageFlagBits pipelineStage, IntPtr queryPool, uint query);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdCopyQueryPoolResults", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdCopyQueryPoolResults(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount, IntPtr dstBuffer, ulong dstOffset, ulong stride, uint flags);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdPushConstants", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdPushConstants(IntPtr commandBuffer, IntPtr layout, uint stageFlags, uint offset, uint size, IntPtr pValues);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdBeginRenderPass", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdBeginRenderPass(IntPtr commandBuffer, IntPtr pRenderPassBegin, SubpassContents contents);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdNextSubpass", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdNextSubpass(IntPtr commandBuffer, SubpassContents contents);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdEndRenderPass", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdEndRenderPass(IntPtr commandBuffer);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdExecuteCommands", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdExecuteCommands(IntPtr commandBuffer, uint commandBufferCount, IntPtr pCommandBuffers);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroySurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroySurfaceKHR(IntPtr instance, IntPtr surface, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceSurfaceSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceSurfaceSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr surface, ref uint pSupported);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceSurfaceCapabilitiesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceSurfaceCapabilitiesKHR(IntPtr physicalDevice, IntPtr surface, IntPtr pSurfaceCapabilities);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceSurfaceFormatsKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceSurfaceFormatsKHR(IntPtr physicalDevice, IntPtr surface, ref uint pSurfaceFormatCount, IntPtr pSurfaceFormats);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceSurfacePresentModesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceSurfacePresentModesKHR(IntPtr physicalDevice, IntPtr surface, ref uint pPresentModeCount, IntPtr pPresentModes);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateSwapchainKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateSwapchainKHR(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSwapchain);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroySwapchainKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroySwapchainKHR(IntPtr device, IntPtr swapchain, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetSwapchainImagesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetSwapchainImagesKHR(IntPtr device, IntPtr swapchain, ref uint pSwapchainImageCount, IntPtr pSwapchainImages);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkAcquireNextImageKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkAcquireNextImageKHR(IntPtr device, IntPtr swapchain, ulong timeout, IntPtr semaphore, IntPtr fence, ref uint pImageIndex);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkQueuePresentKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkQueuePresentKHR(IntPtr queue, IntPtr pPresentInfo);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceDisplayPropertiesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceDisplayPropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceDisplayPlanePropertiesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetPhysicalDeviceDisplayPlanePropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDisplayPlaneSupportedDisplaysKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetDisplayPlaneSupportedDisplaysKHR(IntPtr physicalDevice, uint planeIndex, ref uint pDisplayCount, IntPtr pDisplays);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDisplayModePropertiesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetDisplayModePropertiesKHR(IntPtr physicalDevice, IntPtr display, ref uint pPropertyCount, IntPtr pProperties);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDisplayModeKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDisplayModeKHR(IntPtr physicalDevice, IntPtr display, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pMode);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetDisplayPlaneCapabilitiesKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkGetDisplayPlaneCapabilitiesKHR(IntPtr physicalDevice, IntPtr mode, uint planeIndex, IntPtr pCapabilities);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDisplayPlaneSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDisplayPlaneSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateSharedSwapchainsKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateSharedSwapchainsKHR(IntPtr device, uint swapchainCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pSwapchains);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateXlibSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateXlibSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceXlibPresentationSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern uint vkGetPhysicalDeviceXlibPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr dpy, UInt64 visualID);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateXcbSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateXcbSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceXcbPresentationSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern uint vkGetPhysicalDeviceXcbPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection, UInt32 visual_id);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateWaylandSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateWaylandSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceWaylandPresentationSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern uint vkGetPhysicalDeviceWaylandPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr wl_display);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateMirSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateMirSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceMirPresentationSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern uint vkGetPhysicalDeviceMirPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateAndroidSurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateAndroidSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateWin32SurfaceKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateWin32SurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkGetPhysicalDeviceWin32PresentationSupportKHR", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern uint vkGetPhysicalDeviceWin32PresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCreateDebugReportCallbackEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkCreateDebugReportCallbackEXT(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCallback);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDestroyDebugReportCallbackEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDestroyDebugReportCallbackEXT(IntPtr instance, IntPtr callback, IntPtr pAllocator);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDebugReportMessageEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkDebugReportMessageEXT(IntPtr instance, uint flags, DebugReportObjectTypeEXT objectType, ulong obj, ulong location, int messageCode, string pLayerPrefix, string pMessage);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDebugMarkerSetObjectTagEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkDebugMarkerSetObjectTagEXT(IntPtr device, IntPtr pTagInfo);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkDebugMarkerSetObjectNameEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern Result vkDebugMarkerSetObjectNameEXT(IntPtr device, IntPtr pNameInfo);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDebugMarkerBeginEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDebugMarkerBeginEXT(IntPtr commandBuffer, IntPtr pMarkerInfo);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDebugMarkerEndEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDebugMarkerEndEXT(IntPtr commandBuffer);

		[DllImport(VK_OS_LIB_FILE, EntryPoint = "vkCmdDebugMarkerInsertEXT", ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
		static extern void vkCmdDebugMarkerInsertEXT(IntPtr commandBuffer, IntPtr pMarkerInfo);

	}
}