using System;
using System.Runtime.InteropServices;
using System.Text;

namespace VulkanDotNet
{
	public partial class Vk
	{
		public enum PipelineCacheHeaderVersion
		{
			One = 1,
			BeginRange = One,
			EndRange = One,
			RangeSize = (One - One + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum Result
		{
			Success = 0,
			NotReady = 1,
			Timeout = 2,
			EventSet = 3,
			EventReset = 4,
			Incomplete = 5,
			ErrorOutOfHostMemory = -1,
			ErrorOutOfDeviceMemory = -2,
			ErrorInitializationFailed = -3,
			ErrorDeviceLost = -4,
			ErrorMemoryMapFailed = -5,
			ErrorLayerNotPresent = -6,
			ErrorExtensionNotPresent = -7,
			ErrorFeatureNotPresent = -8,
			ErrorIncompatibleDriver = -9,
			ErrorTooManyObjects = -10,
			ErrorFormatNotSupported = -11,
			ErrorFragmentedPool = -12,
			ErrorSurfaceLostKhr = -1000000000,
			ErrorNativeWindowInUseKhr = -1000000001,
			SuboptimalKhr = 1000001003,
			ErrorOutOfDateKhr = -1000001004,
			ErrorIncompatibleDisplayKhr = -1000003001,
			ErrorValidationFailedExt = -1000011001,
			ErrorInvalidShaderNv = -1000012000,
			BeginRange = ErrorFragmentedPool,
			EndRange = Incomplete,
			RangeSize = (Incomplete - ErrorFragmentedPool + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum StructureType
		{
			ApplicationInfo = 0,
			InstanceCreateInfo = 1,
			DeviceQueueCreateInfo = 2,
			DeviceCreateInfo = 3,
			SubmitInfo = 4,
			MemoryAllocateInfo = 5,
			MappedMemoryRange = 6,
			BindSparseInfo = 7,
			FenceCreateInfo = 8,
			SemaphoreCreateInfo = 9,
			EventCreateInfo = 10,
			QueryPoolCreateInfo = 11,
			BufferCreateInfo = 12,
			BufferViewCreateInfo = 13,
			ImageCreateInfo = 14,
			ImageViewCreateInfo = 15,
			ShaderModuleCreateInfo = 16,
			PipelineCacheCreateInfo = 17,
			PipelineShaderStageCreateInfo = 18,
			PipelineVertexInputStateCreateInfo = 19,
			PipelineInputAssemblyStateCreateInfo = 20,
			PipelineTessellationStateCreateInfo = 21,
			PipelineViewportStateCreateInfo = 22,
			PipelineRasterizationStateCreateInfo = 23,
			PipelineMultisampleStateCreateInfo = 24,
			PipelineDepthStencilStateCreateInfo = 25,
			PipelineColorBlendStateCreateInfo = 26,
			PipelineDynamicStateCreateInfo = 27,
			GraphicsPipelineCreateInfo = 28,
			ComputePipelineCreateInfo = 29,
			PipelineLayoutCreateInfo = 30,
			SamplerCreateInfo = 31,
			DescriptorSetLayoutCreateInfo = 32,
			DescriptorPoolCreateInfo = 33,
			DescriptorSetAllocateInfo = 34,
			WriteDescriptorSet = 35,
			CopyDescriptorSet = 36,
			FramebufferCreateInfo = 37,
			RenderPassCreateInfo = 38,
			CommandPoolCreateInfo = 39,
			CommandBufferAllocateInfo = 40,
			CommandBufferInheritanceInfo = 41,
			CommandBufferBeginInfo = 42,
			RenderPassBeginInfo = 43,
			BufferMemoryBarrier = 44,
			ImageMemoryBarrier = 45,
			MemoryBarrier = 46,
			LoaderInstanceCreateInfo = 47,
			LoaderDeviceCreateInfo = 48,
			SwapchainCreateInfoKhr = 1000001000,
			PresentInfoKhr = 1000001001,
			DisplayModeCreateInfoKhr = 1000002000,
			DisplaySurfaceCreateInfoKhr = 1000002001,
			DisplayPresentInfoKhr = 1000003000,
			XlibSurfaceCreateInfoKhr = 1000004000,
			XcbSurfaceCreateInfoKhr = 1000005000,
			WaylandSurfaceCreateInfoKhr = 1000006000,
			MirSurfaceCreateInfoKhr = 1000007000,
			AndroidSurfaceCreateInfoKhr = 1000008000,
			Win32SurfaceCreateInfoKhr = 1000009000,
			DebugReportCallbackCreateInfoExt = 1000011000,
			PipelineRasterizationStateRasterizationOrderAmd = 1000018000,
			DebugMarkerObjectNameInfoExt = 1000022000,
			DebugMarkerObjectTagInfoExt = 1000022001,
			DebugMarkerMarkerInfoExt = 1000022002,
			DedicatedAllocationImageCreateInfoNv = 1000026000,
			DedicatedAllocationBufferCreateInfoNv = 1000026001,
			DedicatedAllocationMemoryAllocateInfoNv = 1000026002,
			BeginRange = ApplicationInfo,
			EndRange = LoaderDeviceCreateInfo,
			RangeSize = (LoaderDeviceCreateInfo - ApplicationInfo + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum SystemAllocationScope
		{
			Command = 0,
			Object = 1,
			Cache = 2,
			Device = 3,
			Instance = 4,
			BeginRange = Command,
			EndRange = Instance,
			RangeSize = (Instance - Command + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum InternalAllocationType
		{
			Executable = 0,
			BeginRange = Executable,
			EndRange = Executable,
			RangeSize = (Executable - Executable + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum Format
		{
			Undefined = 0,
			R4g4UnormPack8 = 1,
			R4g4b4a4UnormPack16 = 2,
			B4g4r4a4UnormPack16 = 3,
			R5g6b5UnormPack16 = 4,
			B5g6r5UnormPack16 = 5,
			R5g5b5a1UnormPack16 = 6,
			B5g5r5a1UnormPack16 = 7,
			A1r5g5b5UnormPack16 = 8,
			R8Unorm = 9,
			R8Snorm = 10,
			R8Uscaled = 11,
			R8Sscaled = 12,
			R8Uint = 13,
			R8Sint = 14,
			R8Srgb = 15,
			R8g8Unorm = 16,
			R8g8Snorm = 17,
			R8g8Uscaled = 18,
			R8g8Sscaled = 19,
			R8g8Uint = 20,
			R8g8Sint = 21,
			R8g8Srgb = 22,
			R8g8b8Unorm = 23,
			R8g8b8Snorm = 24,
			R8g8b8Uscaled = 25,
			R8g8b8Sscaled = 26,
			R8g8b8Uint = 27,
			R8g8b8Sint = 28,
			R8g8b8Srgb = 29,
			B8g8r8Unorm = 30,
			B8g8r8Snorm = 31,
			B8g8r8Uscaled = 32,
			B8g8r8Sscaled = 33,
			B8g8r8Uint = 34,
			B8g8r8Sint = 35,
			B8g8r8Srgb = 36,
			R8g8b8a8Unorm = 37,
			R8g8b8a8Snorm = 38,
			R8g8b8a8Uscaled = 39,
			R8g8b8a8Sscaled = 40,
			R8g8b8a8Uint = 41,
			R8g8b8a8Sint = 42,
			R8g8b8a8Srgb = 43,
			B8g8r8a8Unorm = 44,
			B8g8r8a8Snorm = 45,
			B8g8r8a8Uscaled = 46,
			B8g8r8a8Sscaled = 47,
			B8g8r8a8Uint = 48,
			B8g8r8a8Sint = 49,
			B8g8r8a8Srgb = 50,
			A8b8g8r8UnormPack32 = 51,
			A8b8g8r8SnormPack32 = 52,
			A8b8g8r8UscaledPack32 = 53,
			A8b8g8r8SscaledPack32 = 54,
			A8b8g8r8UintPack32 = 55,
			A8b8g8r8SintPack32 = 56,
			A8b8g8r8SrgbPack32 = 57,
			A2r10g10b10UnormPack32 = 58,
			A2r10g10b10SnormPack32 = 59,
			A2r10g10b10UscaledPack32 = 60,
			A2r10g10b10SscaledPack32 = 61,
			A2r10g10b10UintPack32 = 62,
			A2r10g10b10SintPack32 = 63,
			A2b10g10r10UnormPack32 = 64,
			A2b10g10r10SnormPack32 = 65,
			A2b10g10r10UscaledPack32 = 66,
			A2b10g10r10SscaledPack32 = 67,
			A2b10g10r10UintPack32 = 68,
			A2b10g10r10SintPack32 = 69,
			R16Unorm = 70,
			R16Snorm = 71,
			R16Uscaled = 72,
			R16Sscaled = 73,
			R16Uint = 74,
			R16Sint = 75,
			R16Sfloat = 76,
			R16g16Unorm = 77,
			R16g16Snorm = 78,
			R16g16Uscaled = 79,
			R16g16Sscaled = 80,
			R16g16Uint = 81,
			R16g16Sint = 82,
			R16g16Sfloat = 83,
			R16g16b16Unorm = 84,
			R16g16b16Snorm = 85,
			R16g16b16Uscaled = 86,
			R16g16b16Sscaled = 87,
			R16g16b16Uint = 88,
			R16g16b16Sint = 89,
			R16g16b16Sfloat = 90,
			R16g16b16a16Unorm = 91,
			R16g16b16a16Snorm = 92,
			R16g16b16a16Uscaled = 93,
			R16g16b16a16Sscaled = 94,
			R16g16b16a16Uint = 95,
			R16g16b16a16Sint = 96,
			R16g16b16a16Sfloat = 97,
			R32Uint = 98,
			R32Sint = 99,
			R32Sfloat = 100,
			R32g32Uint = 101,
			R32g32Sint = 102,
			R32g32Sfloat = 103,
			R32g32b32Uint = 104,
			R32g32b32Sint = 105,
			R32g32b32Sfloat = 106,
			R32g32b32a32Uint = 107,
			R32g32b32a32Sint = 108,
			R32g32b32a32Sfloat = 109,
			R64Uint = 110,
			R64Sint = 111,
			R64Sfloat = 112,
			R64g64Uint = 113,
			R64g64Sint = 114,
			R64g64Sfloat = 115,
			R64g64b64Uint = 116,
			R64g64b64Sint = 117,
			R64g64b64Sfloat = 118,
			R64g64b64a64Uint = 119,
			R64g64b64a64Sint = 120,
			R64g64b64a64Sfloat = 121,
			B10g11r11UfloatPack32 = 122,
			E5b9g9r9UfloatPack32 = 123,
			D16Unorm = 124,
			X8D24UnormPack32 = 125,
			D32Sfloat = 126,
			S8Uint = 127,
			D16UnormS8Uint = 128,
			D24UnormS8Uint = 129,
			D32SfloatS8Uint = 130,
			Bc1RgbUnormBlock = 131,
			Bc1RgbSrgbBlock = 132,
			Bc1RgbaUnormBlock = 133,
			Bc1RgbaSrgbBlock = 134,
			Bc2UnormBlock = 135,
			Bc2SrgbBlock = 136,
			Bc3UnormBlock = 137,
			Bc3SrgbBlock = 138,
			Bc4UnormBlock = 139,
			Bc4SnormBlock = 140,
			Bc5UnormBlock = 141,
			Bc5SnormBlock = 142,
			Bc6hUfloatBlock = 143,
			Bc6hSfloatBlock = 144,
			Bc7UnormBlock = 145,
			Bc7SrgbBlock = 146,
			Etc2R8g8b8UnormBlock = 147,
			Etc2R8g8b8SrgbBlock = 148,
			Etc2R8g8b8a1UnormBlock = 149,
			Etc2R8g8b8a1SrgbBlock = 150,
			Etc2R8g8b8a8UnormBlock = 151,
			Etc2R8g8b8a8SrgbBlock = 152,
			EacR11UnormBlock = 153,
			EacR11SnormBlock = 154,
			EacR11g11UnormBlock = 155,
			EacR11g11SnormBlock = 156,
			Astc4x4UnormBlock = 157,
			Astc4x4SrgbBlock = 158,
			Astc5x4UnormBlock = 159,
			Astc5x4SrgbBlock = 160,
			Astc5x5UnormBlock = 161,
			Astc5x5SrgbBlock = 162,
			Astc6x5UnormBlock = 163,
			Astc6x5SrgbBlock = 164,
			Astc6x6UnormBlock = 165,
			Astc6x6SrgbBlock = 166,
			Astc8x5UnormBlock = 167,
			Astc8x5SrgbBlock = 168,
			Astc8x6UnormBlock = 169,
			Astc8x6SrgbBlock = 170,
			Astc8x8UnormBlock = 171,
			Astc8x8SrgbBlock = 172,
			Astc10x5UnormBlock = 173,
			Astc10x5SrgbBlock = 174,
			Astc10x6UnormBlock = 175,
			Astc10x6SrgbBlock = 176,
			Astc10x8UnormBlock = 177,
			Astc10x8SrgbBlock = 178,
			Astc10x10UnormBlock = 179,
			Astc10x10SrgbBlock = 180,
			Astc12x10UnormBlock = 181,
			Astc12x10SrgbBlock = 182,
			Astc12x12UnormBlock = 183,
			Astc12x12SrgbBlock = 184,
			Pvrtc12bppUnormBlockImg = 1000054000,
			Pvrtc14bppUnormBlockImg = 1000054001,
			Pvrtc22bppUnormBlockImg = 1000054002,
			Pvrtc24bppUnormBlockImg = 1000054003,
			Pvrtc12bppSrgbBlockImg = 1000054004,
			Pvrtc14bppSrgbBlockImg = 1000054005,
			Pvrtc22bppSrgbBlockImg = 1000054006,
			Pvrtc24bppSrgbBlockImg = 1000054007,
			BeginRange = Undefined,
			EndRange = Astc12x12SrgbBlock,
			RangeSize = (Astc12x12SrgbBlock - Undefined + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageType
		{
			Vk1d = 0,
			Vk2d = 1,
			Vk3d = 2,
			BeginRange = Vk1d,
			EndRange = Vk3d,
			RangeSize = (Vk3d - Vk1d + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageTiling
		{
			Optimal = 0,
			Linear = 1,
			BeginRange = Optimal,
			EndRange = Linear,
			RangeSize = (Linear - Optimal + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum PhysicalDeviceType
		{
			Other = 0,
			IntegratedGpu = 1,
			DiscreteGpu = 2,
			VirtualGpu = 3,
			Cpu = 4,
			BeginRange = Other,
			EndRange = Cpu,
			RangeSize = (Cpu - Other + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum QueryType
		{
			Occlusion = 0,
			PipelineStatistics = 1,
			Timestamp = 2,
			BeginRange = Occlusion,
			EndRange = Timestamp,
			RangeSize = (Timestamp - Occlusion + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum SharingMode
		{
			Exclusive = 0,
			Concurrent = 1,
			BeginRange = Exclusive,
			EndRange = Concurrent,
			RangeSize = (Concurrent - Exclusive + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageLayout
		{
			Undefined = 0,
			General = 1,
			ColorAttachmentOptimal = 2,
			DepthStencilAttachmentOptimal = 3,
			DepthStencilReadOnlyOptimal = 4,
			ShaderReadOnlyOptimal = 5,
			TransferSrcOptimal = 6,
			TransferDstOptimal = 7,
			Preinitialized = 8,
			PresentSrcKhr = 1000001002,
			BeginRange = Undefined,
			EndRange = Preinitialized,
			RangeSize = (Preinitialized - Undefined + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageViewType
		{
			Vk1d = 0,
			Vk2d = 1,
			Vk3d = 2,
			Cube = 3,
			Vk1dArray = 4,
			Vk2dArray = 5,
			CubeArray = 6,
			BeginRange = Vk1d,
			EndRange = CubeArray,
			RangeSize = (CubeArray - Vk1d + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum ComponentSwizzle
		{
			Identity = 0,
			Zero = 1,
			One = 2,
			R = 3,
			G = 4,
			B = 5,
			A = 6,
			BeginRange = Identity,
			EndRange = A,
			RangeSize = (A - Identity + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum VertexInputRate
		{
			Vertex = 0,
			Instance = 1,
			BeginRange = Vertex,
			EndRange = Instance,
			RangeSize = (Instance - Vertex + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum PrimitiveTopology
		{
			PointList = 0,
			LineList = 1,
			LineStrip = 2,
			TriangleList = 3,
			TriangleStrip = 4,
			TriangleFan = 5,
			LineListWithAdjacency = 6,
			LineStripWithAdjacency = 7,
			TriangleListWithAdjacency = 8,
			TriangleStripWithAdjacency = 9,
			PatchList = 10,
			BeginRange = PointList,
			EndRange = PatchList,
			RangeSize = (PatchList - PointList + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum PolygonMode
		{
			Fill = 0,
			Line = 1,
			Point = 2,
			BeginRange = Fill,
			EndRange = Point,
			RangeSize = (Point - Fill + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum FrontFace
		{
			CounterClockwise = 0,
			Clockwise = 1,
			BeginRange = CounterClockwise,
			EndRange = Clockwise,
			RangeSize = (Clockwise - CounterClockwise + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum CompareOp
		{
			Never = 0,
			Less = 1,
			Equal = 2,
			LessOrEqual = 3,
			Greater = 4,
			NotEqual = 5,
			GreaterOrEqual = 6,
			Always = 7,
			BeginRange = Never,
			EndRange = Always,
			RangeSize = (Always - Never + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum StencilOp
		{
			Keep = 0,
			Zero = 1,
			Replace = 2,
			IncrementAndClamp = 3,
			DecrementAndClamp = 4,
			Invert = 5,
			IncrementAndWrap = 6,
			DecrementAndWrap = 7,
			BeginRange = Keep,
			EndRange = DecrementAndWrap,
			RangeSize = (DecrementAndWrap - Keep + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum LogicOp
		{
			Clear = 0,
			And = 1,
			AndReverse = 2,
			Copy = 3,
			AndInverted = 4,
			NoOp = 5,
			Xor = 6,
			Or = 7,
			Nor = 8,
			Equivalent = 9,
			Invert = 10,
			OrReverse = 11,
			CopyInverted = 12,
			OrInverted = 13,
			Nand = 14,
			Set = 15,
			BeginRange = Clear,
			EndRange = Set,
			RangeSize = (Set - Clear + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum BlendFactor
		{
			Zero = 0,
			One = 1,
			SrcColor = 2,
			OneMinusSrcColor = 3,
			DstColor = 4,
			OneMinusDstColor = 5,
			SrcAlpha = 6,
			OneMinusSrcAlpha = 7,
			DstAlpha = 8,
			OneMinusDstAlpha = 9,
			ConstantColor = 10,
			OneMinusConstantColor = 11,
			ConstantAlpha = 12,
			OneMinusConstantAlpha = 13,
			SrcAlphaSaturate = 14,
			Src1Color = 15,
			OneMinusSrc1Color = 16,
			Src1Alpha = 17,
			OneMinusSrc1Alpha = 18,
			BeginRange = Zero,
			EndRange = OneMinusSrc1Alpha,
			RangeSize = (OneMinusSrc1Alpha - Zero + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum BlendOp
		{
			Add = 0,
			Subtract = 1,
			ReverseSubtract = 2,
			Min = 3,
			Max = 4,
			BeginRange = Add,
			EndRange = Max,
			RangeSize = (Max - Add + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum DynamicState
		{
			Viewport = 0,
			Scissor = 1,
			LineWidth = 2,
			DepthBias = 3,
			BlendConstants = 4,
			DepthBounds = 5,
			StencilCompareMask = 6,
			StencilWriteMask = 7,
			StencilReference = 8,
			BeginRange = Viewport,
			EndRange = StencilReference,
			RangeSize = (StencilReference - Viewport + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum Filter
		{
			Nearest = 0,
			Linear = 1,
			CubicImg = 1000015000,
			BeginRange = Nearest,
			EndRange = Linear,
			RangeSize = (Linear - Nearest + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum SamplerMipmapMode
		{
			Nearest = 0,
			Linear = 1,
			BeginRange = Nearest,
			EndRange = Linear,
			RangeSize = (Linear - Nearest + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum SamplerAddressMode
		{
			Repeat = 0,
			MirroredRepeat = 1,
			ClampToEdge = 2,
			ClampToBorder = 3,
			MirrorClampToEdge = 4,
			BeginRange = Repeat,
			EndRange = ClampToBorder,
			RangeSize = (ClampToBorder - Repeat + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum BorderColor
		{
			FloatTransparentBlack = 0,
			IntTransparentBlack = 1,
			FloatOpaqueBlack = 2,
			IntOpaqueBlack = 3,
			FloatOpaqueWhite = 4,
			IntOpaqueWhite = 5,
			BeginRange = FloatTransparentBlack,
			EndRange = IntOpaqueWhite,
			RangeSize = (IntOpaqueWhite - FloatTransparentBlack + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum DescriptorType
		{
			Sampler = 0,
			CombinedImageSampler = 1,
			SampledImage = 2,
			StorageImage = 3,
			UniformTexelBuffer = 4,
			StorageTexelBuffer = 5,
			UniformBuffer = 6,
			StorageBuffer = 7,
			UniformBufferDynamic = 8,
			StorageBufferDynamic = 9,
			InputAttachment = 10,
			BeginRange = Sampler,
			EndRange = InputAttachment,
			RangeSize = (InputAttachment - Sampler + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum AttachmentLoadOp
		{
			Load = 0,
			Clear = 1,
			DontCare = 2,
			BeginRange = Load,
			EndRange = DontCare,
			RangeSize = (DontCare - Load + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum AttachmentStoreOp
		{
			Store = 0,
			DontCare = 1,
			BeginRange = Store,
			EndRange = DontCare,
			RangeSize = (DontCare - Store + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum PipelineBindPoint
		{
			Graphics = 0,
			Compute = 1,
			BeginRange = Graphics,
			EndRange = Compute,
			RangeSize = (Compute - Graphics + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum CommandBufferLevel
		{
			Primary = 0,
			Secondary = 1,
			BeginRange = Primary,
			EndRange = Secondary,
			RangeSize = (Secondary - Primary + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum IndexType
		{
			Uint16 = 0,
			Uint32 = 1,
			BeginRange = Uint16,
			EndRange = Uint32,
			RangeSize = (Uint32 - Uint16 + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum SubpassContents
		{
			Inline = 0,
			SecondaryCommandBuffers = 1,
			BeginRange = Inline,
			EndRange = SecondaryCommandBuffers,
			RangeSize = (SecondaryCommandBuffers - Inline + 1),
			MaxEnum = 0x7FFFFFFF
		};

		public enum FormatFeatureFlagBits
		{
			SampledImageBit = 0x00000001,
			StorageImageBit = 0x00000002,
			StorageImageAtomicBit = 0x00000004,
			UniformTexelBufferBit = 0x00000008,
			StorageTexelBufferBit = 0x00000010,
			StorageTexelBufferAtomicBit = 0x00000020,
			VertexBufferBit = 0x00000040,
			ColorAttachmentBit = 0x00000080,
			ColorAttachmentBlendBit = 0x00000100,
			DepthStencilAttachmentBit = 0x00000200,
			BlitSrcBit = 0x00000400,
			BlitDstBit = 0x00000800,
			SampledImageFilterLinearBit = 0x00001000,
			SampledImageFilterCubicBitImg = 0x00002000,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageUsageFlagBits
		{
			TransferSrcBit = 0x00000001,
			TransferDstBit = 0x00000002,
			SampledBit = 0x00000004,
			StorageBit = 0x00000008,
			ColorAttachmentBit = 0x00000010,
			DepthStencilAttachmentBit = 0x00000020,
			TransientAttachmentBit = 0x00000040,
			InputAttachmentBit = 0x00000080,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageCreateFlagBits
		{
			SparseBindingBit = 0x00000001,
			SparseResidencyBit = 0x00000002,
			SparseAliasedBit = 0x00000004,
			MutableFormatBit = 0x00000008,
			CubeCompatibleBit = 0x00000010,
			MaxEnum = 0x7FFFFFFF
		};

		public enum SampleCountFlagBits
		{
			Vk1Bit = 0x00000001,
			Vk2Bit = 0x00000002,
			Vk4Bit = 0x00000004,
			Vk8Bit = 0x00000008,
			Vk16Bit = 0x00000010,
			Vk32Bit = 0x00000020,
			Vk64Bit = 0x00000040,
			MaxEnum = 0x7FFFFFFF
		};

		public enum QueueFlagBits
		{
			GraphicsBit = 0x00000001,
			ComputeBit = 0x00000002,
			TransferBit = 0x00000004,
			SparseBindingBit = 0x00000008,
			MaxEnum = 0x7FFFFFFF
		};

		public enum MemoryPropertyFlagBits
		{
			DeviceLocalBit = 0x00000001,
			HostVisibleBit = 0x00000002,
			HostCoherentBit = 0x00000004,
			HostCachedBit = 0x00000008,
			LazilyAllocatedBit = 0x00000010,
			MaxEnum = 0x7FFFFFFF
		};

		public enum MemoryHeapFlagBits
		{
			DeviceLocalBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum PipelineStageFlagBits
		{
			TopOfPipeBit = 0x00000001,
			DrawIndirectBit = 0x00000002,
			VertexInputBit = 0x00000004,
			VertexShaderBit = 0x00000008,
			TessellationControlShaderBit = 0x00000010,
			TessellationEvaluationShaderBit = 0x00000020,
			GeometryShaderBit = 0x00000040,
			FragmentShaderBit = 0x00000080,
			EarlyFragmentTestsBit = 0x00000100,
			LateFragmentTestsBit = 0x00000200,
			ColorAttachmentOutputBit = 0x00000400,
			ComputeShaderBit = 0x00000800,
			TransferBit = 0x00001000,
			BottomOfPipeBit = 0x00002000,
			HostBit = 0x00004000,
			AllGraphicsBit = 0x00008000,
			AllCommandsBit = 0x00010000,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ImageAspectFlagBits
		{
			ColorBit = 0x00000001,
			DepthBit = 0x00000002,
			StencilBit = 0x00000004,
			MetadataBit = 0x00000008,
			MaxEnum = 0x7FFFFFFF
		};

		public enum SparseImageFormatFlagBits
		{
			SingleMiptailBit = 0x00000001,
			AlignedMipSizeBit = 0x00000002,
			NonstandardBlockSizeBit = 0x00000004,
			MaxEnum = 0x7FFFFFFF
		};

		public enum SparseMemoryBindFlagBits
		{
			MetadataBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum FenceCreateFlagBits
		{
			SignaledBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum QueryPipelineStatisticFlagBits
		{
			InputAssemblyVerticesBit = 0x00000001,
			InputAssemblyPrimitivesBit = 0x00000002,
			VertexShaderInvocationsBit = 0x00000004,
			GeometryShaderInvocationsBit = 0x00000008,
			GeometryShaderPrimitivesBit = 0x00000010,
			ClippingInvocationsBit = 0x00000020,
			ClippingPrimitivesBit = 0x00000040,
			FragmentShaderInvocationsBit = 0x00000080,
			TessellationControlShaderPatchesBit = 0x00000100,
			TessellationEvaluationShaderInvocationsBit = 0x00000200,
			ComputeShaderInvocationsBit = 0x00000400,
			MaxEnum = 0x7FFFFFFF
		};

		public enum QueryResultFlagBits
		{
			Vk64Bit = 0x00000001,
			WaitBit = 0x00000002,
			WithAvailabilityBit = 0x00000004,
			PartialBit = 0x00000008,
			MaxEnum = 0x7FFFFFFF
		};

		public enum BufferCreateFlagBits
		{
			SparseBindingBit = 0x00000001,
			SparseResidencyBit = 0x00000002,
			SparseAliasedBit = 0x00000004,
			MaxEnum = 0x7FFFFFFF
		};

		public enum BufferUsageFlagBits
		{
			TransferSrcBit = 0x00000001,
			TransferDstBit = 0x00000002,
			UniformTexelBufferBit = 0x00000004,
			StorageTexelBufferBit = 0x00000008,
			UniformBufferBit = 0x00000010,
			StorageBufferBit = 0x00000020,
			IndexBufferBit = 0x00000040,
			VertexBufferBit = 0x00000080,
			IndirectBufferBit = 0x00000100,
			MaxEnum = 0x7FFFFFFF
		};

		public enum PipelineCreateFlagBits
		{
			DisableOptimizationBit = 0x00000001,
			AllowDerivativesBit = 0x00000002,
			DerivativeBit = 0x00000004,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ShaderStageFlagBits
		{
			VertexBit = 0x00000001,
			TessellationControlBit = 0x00000002,
			TessellationEvaluationBit = 0x00000004,
			GeometryBit = 0x00000008,
			FragmentBit = 0x00000010,
			ComputeBit = 0x00000020,
			AllGraphics = 0x0000001F,
			All = 0x7FFFFFFF,
			MaxEnum = 0x7FFFFFFF
		};

		public enum CullModeFlagBits
		{
			None = 0,
			FrontBit = 0x00000001,
			BackBit = 0x00000002,
			FrontAndBack = 0x00000003,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ColorComponentFlagBits
		{
			RBit = 0x00000001,
			GBit = 0x00000002,
			BBit = 0x00000004,
			ABit = 0x00000008,
			MaxEnum = 0x7FFFFFFF
		};

		public enum DescriptorPoolCreateFlagBits
		{
			FreeDescriptorSetBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum AttachmentDescriptionFlagBits
		{
			MayAliasBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum AccessFlagBits
		{
			IndirectCommandReadBit = 0x00000001,
			IndexReadBit = 0x00000002,
			VertexAttributeReadBit = 0x00000004,
			UniformReadBit = 0x00000008,
			InputAttachmentReadBit = 0x00000010,
			ShaderReadBit = 0x00000020,
			ShaderWriteBit = 0x00000040,
			ColorAttachmentReadBit = 0x00000080,
			ColorAttachmentWriteBit = 0x00000100,
			DepthStencilAttachmentReadBit = 0x00000200,
			DepthStencilAttachmentWriteBit = 0x00000400,
			TransferReadBit = 0x00000800,
			TransferWriteBit = 0x00001000,
			HostReadBit = 0x00002000,
			HostWriteBit = 0x00004000,
			MemoryReadBit = 0x00008000,
			MemoryWriteBit = 0x00010000,
			MaxEnum = 0x7FFFFFFF
		};

		public enum DependencyFlagBits
		{
			ByRegionBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum CommandPoolCreateFlagBits
		{
			TransientBit = 0x00000001,
			ResetCommandBufferBit = 0x00000002,
			MaxEnum = 0x7FFFFFFF
		};

		public enum CommandPoolResetFlagBits
		{
			ReleaseResourcesBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum CommandBufferUsageFlagBits
		{
			OneTimeSubmitBit = 0x00000001,
			RenderPassContinueBit = 0x00000002,
			SimultaneousUseBit = 0x00000004,
			MaxEnum = 0x7FFFFFFF
		};

		public enum QueryControlFlagBits
		{
			PreciseBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum CommandBufferResetFlagBits
		{
			ReleaseResourcesBit = 0x00000001,
			MaxEnum = 0x7FFFFFFF
		};

		public enum StencilFaceFlagBits
		{
			FrontBit = 0x00000001,
			BackBit = 0x00000002,
			FrontAndBack = 0x00000003,
			MaxEnum = 0x7FFFFFFF
		};

		public enum ColorSpaceKHR
		{
			SrgbNonlinearKhr = 0,
			BeginRangeKhr = SrgbNonlinearKhr,
			EndRangeKhr = SrgbNonlinearKhr,
			RangeSizeKhr = (SrgbNonlinearKhr - SrgbNonlinearKhr + 1),
			MaxEnumKhr = 0x7FFFFFFF
		};

		public enum PresentModeKHR
		{
			ImmediateKhr = 0,
			MailboxKhr = 1,
			FifoKhr = 2,
			FifoRelaxedKhr = 3,
			BeginRangeKhr = ImmediateKhr,
			EndRangeKhr = FifoRelaxedKhr,
			RangeSizeKhr = (FifoRelaxedKhr - ImmediateKhr + 1),
			MaxEnumKhr = 0x7FFFFFFF
		};

		public enum SurfaceTransformFlagBitsKHR
		{
			IdentityBitKhr = 0x00000001,
			Rotate90BitKhr = 0x00000002,
			Rotate180BitKhr = 0x00000004,
			Rotate270BitKhr = 0x00000008,
			HorizontalMirrorBitKhr = 0x00000010,
			HorizontalMirrorRotate90BitKhr = 0x00000020,
			HorizontalMirrorRotate180BitKhr = 0x00000040,
			HorizontalMirrorRotate270BitKhr = 0x00000080,
			InheritBitKhr = 0x00000100,
			MaxEnumKhr = 0x7FFFFFFF
		};

		public enum CompositeAlphaFlagBitsKHR
		{
			OpaqueBitKhr = 0x00000001,
			PreMultipliedBitKhr = 0x00000002,
			PostMultipliedBitKhr = 0x00000004,
			InheritBitKhr = 0x00000008,
			MaxEnumKhr = 0x7FFFFFFF
		};

		public enum DisplayPlaneAlphaFlagBitsKHR
		{
			OpaqueBitKhr = 0x00000001,
			GlobalBitKhr = 0x00000002,
			PerPixelBitKhr = 0x00000004,
			PerPixelPremultipliedBitKhr = 0x00000008,
			MaxEnumKhr = 0x7FFFFFFF
		};

		public enum DebugReportObjectTypeEXT
		{
			UnknownExt = 0,
			InstanceExt = 1,
			PhysicalDeviceExt = 2,
			DeviceExt = 3,
			QueueExt = 4,
			SemaphoreExt = 5,
			CommandBufferExt = 6,
			FenceExt = 7,
			DeviceMemoryExt = 8,
			BufferExt = 9,
			ImageExt = 10,
			EventExt = 11,
			QueryPoolExt = 12,
			BufferViewExt = 13,
			ImageViewExt = 14,
			ShaderModuleExt = 15,
			PipelineCacheExt = 16,
			PipelineLayoutExt = 17,
			RenderPassExt = 18,
			PipelineExt = 19,
			DescriptorSetLayoutExt = 20,
			SamplerExt = 21,
			DescriptorPoolExt = 22,
			DescriptorSetExt = 23,
			FramebufferExt = 24,
			CommandPoolExt = 25,
			SurfaceKhrExt = 26,
			SwapchainKhrExt = 27,
			DebugReportExt = 28,
			BeginRangeExt = UnknownExt,
			EndRangeExt = DebugReportExt,
			RangeSizeExt = (DebugReportExt - UnknownExt + 1),
			MaxEnumExt = 0x7FFFFFFF
		};

		public enum DebugReportErrorEXT
		{
			NoneExt = 0,
			CallbackRefExt = 1,
			BeginRangeExt = NoneExt,
			EndRangeExt = CallbackRefExt,
			RangeSizeExt = (CallbackRefExt - NoneExt + 1),
			MaxEnumExt = 0x7FFFFFFF
		};

		public enum DebugReportFlagBitsEXT
		{
			InformationBitExt = 0x00000001,
			WarningBitExt = 0x00000002,
			PerformanceWarningBitExt = 0x00000004,
			ErrorBitExt = 0x00000008,
			DebugBitExt = 0x00000010,
			MaxEnumExt = 0x7FFFFFFF
		};

		public enum RasterizationOrderAMD
		{
			StrictAmd = 0,
			RelaxedAmd = 1,
			BeginRangeAmd = StrictAmd,
			EndRangeAmd = RelaxedAmd,
			RangeSizeAmd = (RelaxedAmd - StrictAmd + 1),
			MaxEnumAmd = 0x7FFFFFFF
		};

	}
}