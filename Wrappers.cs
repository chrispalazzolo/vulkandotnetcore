using System;
using System.Runtime.InteropServices;
using System.Text;

namespace VulkanDotNet
{
	public partial class Vk
	{
		public static Result CreateInstance(IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pInstance) => vkCreateInstance(pCreateInfo, pAllocator, pInstance);
		public static void DestroyInstance(IntPtr instance, IntPtr pAllocator){ vkDestroyInstance(instance, pAllocator);}
		public static Result EnumeratePhysicalDevices(IntPtr instance, ref uint pPhysicalDeviceCount, IntPtr pPhysicalDevices) => vkEnumeratePhysicalDevices(instance, ref pPhysicalDeviceCount, pPhysicalDevices);
		public static void GetPhysicalDeviceFeatures(IntPtr physicalDevice, IntPtr pFeatures){ vkGetPhysicalDeviceFeatures(physicalDevice, pFeatures);}
		public static void GetPhysicalDeviceFormatProperties(IntPtr physicalDevice, Format format, IntPtr pFormatProperties){ vkGetPhysicalDeviceFormatProperties(physicalDevice, format, pFormatProperties);}
		public static Result GetPhysicalDeviceImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, ImageTiling tiling, uint usage, uint flags, IntPtr pImageFormatProperties) => vkGetPhysicalDeviceImageFormatProperties(physicalDevice, format, type, tiling, usage, flags, pImageFormatProperties);
		public static void GetPhysicalDeviceProperties(IntPtr physicalDevice, IntPtr pProperties){ vkGetPhysicalDeviceProperties(physicalDevice, pProperties);}
		public static void GetPhysicalDeviceQueueFamilyProperties(IntPtr physicalDevice, ref uint pQueueFamilyPropertyCount, IntPtr pQueueFamilyProperties){ vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, ref pQueueFamilyPropertyCount, pQueueFamilyProperties);}
		public static void GetPhysicalDeviceMemoryProperties(IntPtr physicalDevice, IntPtr pMemoryProperties){ vkGetPhysicalDeviceMemoryProperties(physicalDevice, pMemoryProperties);}
		public static PFN_vkVoidFunction GetInstanceProcAddr(IntPtr instance, string pName) => vkGetInstanceProcAddr(instance, pName);
		public static PFN_vkVoidFunction GetDeviceProcAddr(IntPtr device, string pName) => vkGetDeviceProcAddr(device, pName);
		public static Result CreateDevice(IntPtr physicalDevice, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDevice) => vkCreateDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);
		public static void DestroyDevice(IntPtr device, IntPtr pAllocator){ vkDestroyDevice(device, pAllocator);}
		public static Result EnumerateInstanceExtensionProperties(string pLayerName, ref uint pPropertyCount, IntPtr pProperties) => vkEnumerateInstanceExtensionProperties(pLayerName, ref pPropertyCount, pProperties);
		public static Result EnumerateDeviceExtensionProperties(IntPtr physicalDevice, string pLayerName, ref uint pPropertyCount, IntPtr pProperties) => vkEnumerateDeviceExtensionProperties(physicalDevice, pLayerName, ref pPropertyCount, pProperties);
		public static Result EnumerateInstanceLayerProperties(ref uint pPropertyCount, IntPtr pProperties) => vkEnumerateInstanceLayerProperties(ref pPropertyCount, pProperties);
		public static Result EnumerateDeviceLayerProperties(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties) => vkEnumerateDeviceLayerProperties(physicalDevice, ref pPropertyCount, pProperties);
		public static void GetDeviceQueue(IntPtr device, uint queueFamilyIndex, uint queueIndex, IntPtr pQueue){ vkGetDeviceQueue(device, queueFamilyIndex, queueIndex, pQueue);}
		public static Result QueueSubmit(IntPtr queue, uint submitCount, IntPtr pSubmits, IntPtr fence) => vkQueueSubmit(queue, submitCount, pSubmits, fence);
		public static Result QueueWaitIdle(IntPtr queue) => vkQueueWaitIdle(queue);
		public static Result DeviceWaitIdle(IntPtr device) => vkDeviceWaitIdle(device);
		public static Result AllocateMemory(IntPtr device, IntPtr pAllocateInfo, IntPtr pAllocator, IntPtr pMemory) => vkAllocateMemory(device, pAllocateInfo, pAllocator, pMemory);
		public static void FreeMemory(IntPtr device, IntPtr memory, IntPtr pAllocator){ vkFreeMemory(device, memory, pAllocator);}
		public static Result MapMemory(IntPtr device, IntPtr memory, ulong offset, ulong size, uint flags, IntPtr ppData) => vkMapMemory(device, memory, offset, size, flags, ppData);
		public static void UnmapMemory(IntPtr device, IntPtr memory){ vkUnmapMemory(device, memory);}
		public static Result FlushMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges) => vkFlushMappedMemoryRanges(device, memoryRangeCount, pMemoryRanges);
		public static Result InvalidateMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges) => vkInvalidateMappedMemoryRanges(device, memoryRangeCount, pMemoryRanges);
		public static void GetDeviceMemoryCommitment(IntPtr device, IntPtr memory, ref ulong pCommittedMemoryInBytes){ vkGetDeviceMemoryCommitment(device, memory, ref pCommittedMemoryInBytes);}
		public static Result BindBufferMemory(IntPtr device, IntPtr buffer, IntPtr memory, ulong memoryOffset) => vkBindBufferMemory(device, buffer, memory, memoryOffset);
		public static Result BindImageMemory(IntPtr device, IntPtr image, IntPtr memory, ulong memoryOffset) => vkBindImageMemory(device, image, memory, memoryOffset);
		public static void GetBufferMemoryRequirements(IntPtr device, IntPtr buffer, IntPtr pMemoryRequirements){ vkGetBufferMemoryRequirements(device, buffer, pMemoryRequirements);}
		public static void GetImageMemoryRequirements(IntPtr device, IntPtr image, IntPtr pMemoryRequirements){ vkGetImageMemoryRequirements(device, image, pMemoryRequirements);}
		public static void GetImageSparseMemoryRequirements(IntPtr device, IntPtr image, ref uint pSparseMemoryRequirementCount, IntPtr pSparseMemoryRequirements){ vkGetImageSparseMemoryRequirements(device, image, ref pSparseMemoryRequirementCount, pSparseMemoryRequirements);}
		public static void GetPhysicalDeviceSparseImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, SampleCountFlagBits samples, uint usage, ImageTiling tiling, ref uint pPropertyCount, IntPtr pProperties){ vkGetPhysicalDeviceSparseImageFormatProperties(physicalDevice, format, type, samples, usage, tiling, ref pPropertyCount, pProperties);}
		public static Result QueueBindSparse(IntPtr queue, uint bindInfoCount, IntPtr pBindInfo, IntPtr fence) => vkQueueBindSparse(queue, bindInfoCount, pBindInfo, fence);
		public static Result CreateFence(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFence) => vkCreateFence(device, pCreateInfo, pAllocator, pFence);
		public static void DestroyFence(IntPtr device, IntPtr fence, IntPtr pAllocator){ vkDestroyFence(device, fence, pAllocator);}
		public static Result ResetFences(IntPtr device, uint fenceCount, IntPtr pFences) => vkResetFences(device, fenceCount, pFences);
		public static Result GetFenceStatus(IntPtr device, IntPtr fence) => vkGetFenceStatus(device, fence);
		public static Result WaitForFences(IntPtr device, uint fenceCount, IntPtr pFences, uint waitAll, ulong timeout) => vkWaitForFences(device, fenceCount, pFences, waitAll, timeout);
		public static Result CreateSemaphore(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSemaphore) => vkCreateSemaphore(device, pCreateInfo, pAllocator, pSemaphore);
		public static void DestroySemaphore(IntPtr device, IntPtr semaphore, IntPtr pAllocator){ vkDestroySemaphore(device, semaphore, pAllocator);}
		public static Result CreateEvent(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pEvent) => vkCreateEvent(device, pCreateInfo, pAllocator, pEvent);
		public static void DestroyEvent(IntPtr device, IntPtr evnt, IntPtr pAllocator){ vkDestroyEvent(device, evnt, pAllocator);}
		public static Result GetEventStatus(IntPtr device, IntPtr evnt) => vkGetEventStatus(device, evnt);
		public static Result SetEvent(IntPtr device, IntPtr evnt) => vkSetEvent(device, evnt);
		public static Result ResetEvent(IntPtr device, IntPtr evnt) => vkResetEvent(device, evnt);
		public static Result CreateQueryPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pQueryPool) => vkCreateQueryPool(device, pCreateInfo, pAllocator, pQueryPool);
		public static void DestroyQueryPool(IntPtr device, IntPtr queryPool, IntPtr pAllocator){ vkDestroyQueryPool(device, queryPool, pAllocator);}
		public static Result GetQueryPoolResults(IntPtr device, IntPtr queryPool, uint firstQuery, uint queryCount, ulong dataSize, IntPtr pData, ulong stride, uint flags) => vkGetQueryPoolResults(device, queryPool, firstQuery, queryCount, dataSize, pData, stride, flags);
		public static Result CreateBuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pBuffer) => vkCreateBuffer(device, pCreateInfo, pAllocator, pBuffer);
		public static void DestroyBuffer(IntPtr device, IntPtr buffer, IntPtr pAllocator){ vkDestroyBuffer(device, buffer, pAllocator);}
		public static Result CreateBufferView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView) => vkCreateBufferView(device, pCreateInfo, pAllocator, pView);
		public static void DestroyBufferView(IntPtr device, IntPtr bufferView, IntPtr pAllocator){ vkDestroyBufferView(device, bufferView, pAllocator);}
		public static Result CreateImage(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pImage) => vkCreateImage(device, pCreateInfo, pAllocator, pImage);
		public static void DestroyImage(IntPtr device, IntPtr image, IntPtr pAllocator){ vkDestroyImage(device, image, pAllocator);}
		public static void GetImageSubresourceLayout(IntPtr device, IntPtr image, IntPtr pSubresource, IntPtr pLayout){ vkGetImageSubresourceLayout(device, image, pSubresource, pLayout);}
		public static Result CreateImageView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView) => vkCreateImageView(device, pCreateInfo, pAllocator, pView);
		public static void DestroyImageView(IntPtr device, IntPtr imageView, IntPtr pAllocator){ vkDestroyImageView(device, imageView, pAllocator);}
		public static Result CreateShaderModule(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pShaderModule) => vkCreateShaderModule(device, pCreateInfo, pAllocator, pShaderModule);
		public static void DestroyShaderModule(IntPtr device, IntPtr shaderModule, IntPtr pAllocator){ vkDestroyShaderModule(device, shaderModule, pAllocator);}
		public static Result CreatePipelineCache(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineCache) => vkCreatePipelineCache(device, pCreateInfo, pAllocator, pPipelineCache);
		public static void DestroyPipelineCache(IntPtr device, IntPtr pipelineCache, IntPtr pAllocator){ vkDestroyPipelineCache(device, pipelineCache, pAllocator);}
		public static Result GetPipelineCacheData(IntPtr device, IntPtr pipelineCache, ref ulong pDataSize, IntPtr pData) => vkGetPipelineCacheData(device, pipelineCache, ref pDataSize, pData);
		public static Result MergePipelineCaches(IntPtr device, IntPtr dstCache, uint srcCacheCount, IntPtr pSrcCaches) => vkMergePipelineCaches(device, dstCache, srcCacheCount, pSrcCaches);
		public static Result CreateGraphicsPipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines) => vkCreateGraphicsPipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
		public static Result CreateComputePipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines) => vkCreateComputePipelines(device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
		public static void DestroyPipeline(IntPtr device, IntPtr pipeline, IntPtr pAllocator){ vkDestroyPipeline(device, pipeline, pAllocator);}
		public static Result CreatePipelineLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineLayout) => vkCreatePipelineLayout(device, pCreateInfo, pAllocator, pPipelineLayout);
		public static void DestroyPipelineLayout(IntPtr device, IntPtr pipelineLayout, IntPtr pAllocator){ vkDestroyPipelineLayout(device, pipelineLayout, pAllocator);}
		public static Result CreateSampler(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSampler) => vkCreateSampler(device, pCreateInfo, pAllocator, pSampler);
		public static void DestroySampler(IntPtr device, IntPtr sampler, IntPtr pAllocator){ vkDestroySampler(device, sampler, pAllocator);}
		public static Result CreateDescriptorSetLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSetLayout) => vkCreateDescriptorSetLayout(device, pCreateInfo, pAllocator, pSetLayout);
		public static void DestroyDescriptorSetLayout(IntPtr device, IntPtr descriptorSetLayout, IntPtr pAllocator){ vkDestroyDescriptorSetLayout(device, descriptorSetLayout, pAllocator);}
		public static Result CreateDescriptorPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDescriptorPool) => vkCreateDescriptorPool(device, pCreateInfo, pAllocator, pDescriptorPool);
		public static void DestroyDescriptorPool(IntPtr device, IntPtr descriptorPool, IntPtr pAllocator){ vkDestroyDescriptorPool(device, descriptorPool, pAllocator);}
		public static Result ResetDescriptorPool(IntPtr device, IntPtr descriptorPool, uint flags) => vkResetDescriptorPool(device, descriptorPool, flags);
		public static Result AllocateDescriptorSets(IntPtr device, IntPtr pAllocateInfo, IntPtr pDescriptorSets) => vkAllocateDescriptorSets(device, pAllocateInfo, pDescriptorSets);
		public static Result FreeDescriptorSets(IntPtr device, IntPtr descriptorPool, uint descriptorSetCount, IntPtr pDescriptorSets) => vkFreeDescriptorSets(device, descriptorPool, descriptorSetCount, pDescriptorSets);
		public static void UpdateDescriptorSets(IntPtr device, uint descriptorWriteCount, IntPtr pDescriptorWrites, uint descriptorCopyCount, IntPtr pDescriptorCopies){ vkUpdateDescriptorSets(device, descriptorWriteCount, pDescriptorWrites, descriptorCopyCount, pDescriptorCopies);}
		public static Result CreateFramebuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFramebuffer) => vkCreateFramebuffer(device, pCreateInfo, pAllocator, pFramebuffer);
		public static void DestroyFramebuffer(IntPtr device, IntPtr framebuffer, IntPtr pAllocator){ vkDestroyFramebuffer(device, framebuffer, pAllocator);}
		public static Result CreateRenderPass(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pRenderPass) => vkCreateRenderPass(device, pCreateInfo, pAllocator, pRenderPass);
		public static void DestroyRenderPass(IntPtr device, IntPtr renderPass, IntPtr pAllocator){ vkDestroyRenderPass(device, renderPass, pAllocator);}
		public static void GetRenderAreaGranularity(IntPtr device, IntPtr renderPass, IntPtr pGranularity){ vkGetRenderAreaGranularity(device, renderPass, pGranularity);}
		public static Result CreateCommandPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCommandPool) => vkCreateCommandPool(device, pCreateInfo, pAllocator, pCommandPool);
		public static void DestroyCommandPool(IntPtr device, IntPtr commandPool, IntPtr pAllocator){ vkDestroyCommandPool(device, commandPool, pAllocator);}
		public static Result ResetCommandPool(IntPtr device, IntPtr commandPool, uint flags) => vkResetCommandPool(device, commandPool, flags);
		public static Result AllocateCommandBuffers(IntPtr device, IntPtr pAllocateInfo, IntPtr pCommandBuffers) => vkAllocateCommandBuffers(device, pAllocateInfo, pCommandBuffers);
		public static void FreeCommandBuffers(IntPtr device, IntPtr commandPool, uint commandBufferCount, IntPtr pCommandBuffers){ vkFreeCommandBuffers(device, commandPool, commandBufferCount, pCommandBuffers);}
		public static Result BeginCommandBuffer(IntPtr commandBuffer, IntPtr pBeginInfo) => vkBeginCommandBuffer(commandBuffer, pBeginInfo);
		public static Result EndCommandBuffer(IntPtr commandBuffer) => vkEndCommandBuffer(commandBuffer);
		public static Result ResetCommandBuffer(IntPtr commandBuffer, uint flags) => vkResetCommandBuffer(commandBuffer, flags);
		public static void CmdBindPipeline(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr pipeline){ vkCmdBindPipeline(commandBuffer, pipelineBindPoint, pipeline);}
		public static void CmdSetViewport(IntPtr commandBuffer, uint firstViewport, uint viewportCount, IntPtr pViewports){ vkCmdSetViewport(commandBuffer, firstViewport, viewportCount, pViewports);}
		public static void CmdSetScissor(IntPtr commandBuffer, uint firstScissor, uint scissorCount, IntPtr pScissors){ vkCmdSetScissor(commandBuffer, firstScissor, scissorCount, pScissors);}
		public static void CmdSetLineWidth(IntPtr commandBuffer, float lineWidth){ vkCmdSetLineWidth(commandBuffer, lineWidth);}
		public static void CmdSetDepthBias(IntPtr commandBuffer, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor){ vkCmdSetDepthBias(commandBuffer, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);}
		public static void CmdSetBlendConstants(IntPtr commandBuffer, float[] blendConstants){ vkCmdSetBlendConstants(commandBuffer, blendConstants);}
		public static void CmdSetDepthBounds(IntPtr commandBuffer, float minDepthBounds, float maxDepthBounds){ vkCmdSetDepthBounds(commandBuffer, minDepthBounds, maxDepthBounds);}
		public static void CmdSetStencilCompareMask(IntPtr commandBuffer, uint faceMask, uint compareMask){ vkCmdSetStencilCompareMask(commandBuffer, faceMask, compareMask);}
		public static void CmdSetStencilWriteMask(IntPtr commandBuffer, uint faceMask, uint writeMask){ vkCmdSetStencilWriteMask(commandBuffer, faceMask, writeMask);}
		public static void CmdSetStencilReference(IntPtr commandBuffer, uint faceMask, uint reference){ vkCmdSetStencilReference(commandBuffer, faceMask, reference);}
		public static void CmdBindDescriptorSets(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr layout, uint firstSet, uint descriptorSetCount, IntPtr pDescriptorSets, uint dynamicOffsetCount, ref uint pDynamicOffsets){ vkCmdBindDescriptorSets(commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount, pDescriptorSets, dynamicOffsetCount, ref pDynamicOffsets);}
		public static void CmdBindIndexBuffer(IntPtr commandBuffer, IntPtr buffer, ulong offset, IndexType indexType){ vkCmdBindIndexBuffer(commandBuffer, buffer, offset, indexType);}
		public static void CmdBindVertexBuffers(IntPtr commandBuffer, uint firstBinding, uint bindingCount, IntPtr pBuffers, ref ulong pOffsets){ vkCmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, pBuffers, ref pOffsets);}
		public static void CmdDraw(IntPtr commandBuffer, uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance){ vkCmdDraw(commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);}
		public static void CmdDrawIndexed(IntPtr commandBuffer, uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance){ vkCmdDrawIndexed(commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);}
		public static void CmdDrawIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride){ vkCmdDrawIndirect(commandBuffer, buffer, offset, drawCount, stride);}
		public static void CmdDrawIndexedIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride){ vkCmdDrawIndexedIndirect(commandBuffer, buffer, offset, drawCount, stride);}
		public static void CmdDispatch(IntPtr commandBuffer, uint x, uint y, uint z){ vkCmdDispatch(commandBuffer, x, y, z);}
		public static void CmdDispatchIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset){ vkCmdDispatchIndirect(commandBuffer, buffer, offset);}
		public static void CmdCopyBuffer(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstBuffer, uint regionCount, IntPtr pRegions){ vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, regionCount, pRegions);}
		public static void CmdCopyImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions){ vkCmdCopyImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);}
		public static void CmdBlitImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions, Filter filter){ vkCmdBlitImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter);}
		public static void CmdCopyBufferToImage(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions){ vkCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);}
		public static void CmdCopyImageToBuffer(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstBuffer, uint regionCount, IntPtr pRegions){ vkCmdCopyImageToBuffer(commandBuffer, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);}
		public static void CmdUpdateBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong dataSize, IntPtr pData){ vkCmdUpdateBuffer(commandBuffer, dstBuffer, dstOffset, dataSize, pData);}
		public static void CmdFillBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong size, uint data){ vkCmdFillBuffer(commandBuffer, dstBuffer, dstOffset, size, data);}
		public static void CmdClearColorImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pColor, uint rangeCount, IntPtr pRanges){ vkCmdClearColorImage(commandBuffer, image, imageLayout, pColor, rangeCount, pRanges);}
		public static void CmdClearDepthStencilImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pDepthStencil, uint rangeCount, IntPtr pRanges){ vkCmdClearDepthStencilImage(commandBuffer, image, imageLayout, pDepthStencil, rangeCount, pRanges);}
		public static void CmdClearAttachments(IntPtr commandBuffer, uint attachmentCount, IntPtr pAttachments, uint rectCount, IntPtr pRects){ vkCmdClearAttachments(commandBuffer, attachmentCount, pAttachments, rectCount, pRects);}
		public static void CmdResolveImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions){ vkCmdResolveImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);}
		public static void CmdSetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask){ vkCmdSetEvent(commandBuffer, evnt, stageMask);}
		public static void CmdResetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask){ vkCmdResetEvent(commandBuffer, evnt, stageMask);}
		public static void CmdWaitEvents(IntPtr commandBuffer, uint eventCount, IntPtr pEvents, uint srcStageMask, uint dstStageMask, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers){ vkCmdWaitEvents(commandBuffer, eventCount, pEvents, srcStageMask, dstStageMask, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);}
		public static void CmdPipelineBarrier(IntPtr commandBuffer, uint srcStageMask, uint dstStageMask, uint dependencyFlags, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers){ vkCmdPipelineBarrier(commandBuffer, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);}
		public static void CmdBeginQuery(IntPtr commandBuffer, IntPtr queryPool, uint query, uint flags){ vkCmdBeginQuery(commandBuffer, queryPool, query, flags);}
		public static void CmdEndQuery(IntPtr commandBuffer, IntPtr queryPool, uint query){ vkCmdEndQuery(commandBuffer, queryPool, query);}
		public static void CmdResetQueryPool(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount){ vkCmdResetQueryPool(commandBuffer, queryPool, firstQuery, queryCount);}
		public static void CmdWriteTimestamp(IntPtr commandBuffer, PipelineStageFlagBits pipelineStage, IntPtr queryPool, uint query){ vkCmdWriteTimestamp(commandBuffer, pipelineStage, queryPool, query);}
		public static void CmdCopyQueryPoolResults(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount, IntPtr dstBuffer, ulong dstOffset, ulong stride, uint flags){ vkCmdCopyQueryPoolResults(commandBuffer, queryPool, firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);}
		public static void CmdPushConstants(IntPtr commandBuffer, IntPtr layout, uint stageFlags, uint offset, uint size, IntPtr pValues){ vkCmdPushConstants(commandBuffer, layout, stageFlags, offset, size, pValues);}
		public static void CmdBeginRenderPass(IntPtr commandBuffer, IntPtr pRenderPassBegin, SubpassContents contents){ vkCmdBeginRenderPass(commandBuffer, pRenderPassBegin, contents);}
		public static void CmdNextSubpass(IntPtr commandBuffer, SubpassContents contents){ vkCmdNextSubpass(commandBuffer, contents);}
		public static void CmdEndRenderPass(IntPtr commandBuffer){ vkCmdEndRenderPass(commandBuffer);}
		public static void CmdExecuteCommands(IntPtr commandBuffer, uint commandBufferCount, IntPtr pCommandBuffers){ vkCmdExecuteCommands(commandBuffer, commandBufferCount, pCommandBuffers);}
		public static void DestroySurfaceKHR(IntPtr instance, IntPtr surface, IntPtr pAllocator){ vkDestroySurfaceKHR(instance, surface, pAllocator);}
		public static Result GetPhysicalDeviceSurfaceSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr surface, ref uint pSupported) => vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, ref pSupported);
		public static Result GetPhysicalDeviceSurfaceCapabilitiesKHR(IntPtr physicalDevice, IntPtr surface, IntPtr pSurfaceCapabilities) => vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, pSurfaceCapabilities);
		public static Result GetPhysicalDeviceSurfaceFormatsKHR(IntPtr physicalDevice, IntPtr surface, ref uint pSurfaceFormatCount, IntPtr pSurfaceFormats) => vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, ref pSurfaceFormatCount, pSurfaceFormats);
		public static Result GetPhysicalDeviceSurfacePresentModesKHR(IntPtr physicalDevice, IntPtr surface, ref uint pPresentModeCount, IntPtr pPresentModes) => vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, ref pPresentModeCount, pPresentModes);
		public static Result CreateSwapchainKHR(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSwapchain) => vkCreateSwapchainKHR(device, pCreateInfo, pAllocator, pSwapchain);
		public static void DestroySwapchainKHR(IntPtr device, IntPtr swapchain, IntPtr pAllocator){ vkDestroySwapchainKHR(device, swapchain, pAllocator);}
		public static Result GetSwapchainImagesKHR(IntPtr device, IntPtr swapchain, ref uint pSwapchainImageCount, IntPtr pSwapchainImages) => vkGetSwapchainImagesKHR(device, swapchain, ref pSwapchainImageCount, pSwapchainImages);
		public static Result AcquireNextImageKHR(IntPtr device, IntPtr swapchain, ulong timeout, IntPtr semaphore, IntPtr fence, ref uint pImageIndex) => vkAcquireNextImageKHR(device, swapchain, timeout, semaphore, fence, ref pImageIndex);
		public static Result QueuePresentKHR(IntPtr queue, IntPtr pPresentInfo) => vkQueuePresentKHR(queue, pPresentInfo);
		public static Result GetPhysicalDeviceDisplayPropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties) => vkGetPhysicalDeviceDisplayPropertiesKHR(physicalDevice, ref pPropertyCount, pProperties);
		public static Result GetPhysicalDeviceDisplayPlanePropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties) => vkGetPhysicalDeviceDisplayPlanePropertiesKHR(physicalDevice, ref pPropertyCount, pProperties);
		public static Result GetDisplayPlaneSupportedDisplaysKHR(IntPtr physicalDevice, uint planeIndex, ref uint pDisplayCount, IntPtr pDisplays) => vkGetDisplayPlaneSupportedDisplaysKHR(physicalDevice, planeIndex, ref pDisplayCount, pDisplays);
		public static Result GetDisplayModePropertiesKHR(IntPtr physicalDevice, IntPtr display, ref uint pPropertyCount, IntPtr pProperties) => vkGetDisplayModePropertiesKHR(physicalDevice, display, ref pPropertyCount, pProperties);
		public static Result CreateDisplayModeKHR(IntPtr physicalDevice, IntPtr display, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pMode) => vkCreateDisplayModeKHR(physicalDevice, display, pCreateInfo, pAllocator, pMode);
		public static Result GetDisplayPlaneCapabilitiesKHR(IntPtr physicalDevice, IntPtr mode, uint planeIndex, IntPtr pCapabilities) => vkGetDisplayPlaneCapabilitiesKHR(physicalDevice, mode, planeIndex, pCapabilities);
		public static Result CreateDisplayPlaneSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateDisplayPlaneSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static Result CreateSharedSwapchainsKHR(IntPtr device, uint swapchainCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pSwapchains) => vkCreateSharedSwapchainsKHR(device, swapchainCount, pCreateInfos, pAllocator, pSwapchains);
		public static Result CreateXlibSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateXlibSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static uint GetPhysicalDeviceXlibPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr dpy, UInt64 visualID) => vkGetPhysicalDeviceXlibPresentationSupportKHR(physicalDevice, queueFamilyIndex, dpy, visualID);
		public static Result CreateXcbSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateXcbSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static uint GetPhysicalDeviceXcbPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection, UInt32 visual_id) => vkGetPhysicalDeviceXcbPresentationSupportKHR(physicalDevice, queueFamilyIndex, connection, visual_id);
		public static Result CreateWaylandSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateWaylandSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static uint GetPhysicalDeviceWaylandPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr wl_display) => vkGetPhysicalDeviceWaylandPresentationSupportKHR(physicalDevice, queueFamilyIndex, wl_display);
		public static Result CreateMirSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateMirSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static uint GetPhysicalDeviceMirPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection) => vkGetPhysicalDeviceMirPresentationSupportKHR(physicalDevice, queueFamilyIndex, connection);
		public static Result CreateAndroidSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateAndroidSurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static Result CreateWin32SurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface) => vkCreateWin32SurfaceKHR(instance, pCreateInfo, pAllocator, pSurface);
		public static uint GetPhysicalDeviceWin32PresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex) => vkGetPhysicalDeviceWin32PresentationSupportKHR(physicalDevice, queueFamilyIndex);
		public static Result CreateDebugReportCallbackEXT(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCallback) => vkCreateDebugReportCallbackEXT(instance, pCreateInfo, pAllocator, pCallback);
		public static void DestroyDebugReportCallbackEXT(IntPtr instance, IntPtr callback, IntPtr pAllocator){ vkDestroyDebugReportCallbackEXT(instance, callback, pAllocator);}
		public static void DebugReportMessageEXT(IntPtr instance, uint flags, DebugReportObjectTypeEXT objectType, ulong obj, ulong location, int messageCode, string pLayerPrefix, string pMessage){ vkDebugReportMessageEXT(instance, flags, objectType, obj, location, messageCode, pLayerPrefix, pMessage);}
		public static Result DebugMarkerSetObjectTagEXT(IntPtr device, IntPtr pTagInfo) => vkDebugMarkerSetObjectTagEXT(device, pTagInfo);
		public static Result DebugMarkerSetObjectNameEXT(IntPtr device, IntPtr pNameInfo) => vkDebugMarkerSetObjectNameEXT(device, pNameInfo);
		public static void CmdDebugMarkerBeginEXT(IntPtr commandBuffer, IntPtr pMarkerInfo){ vkCmdDebugMarkerBeginEXT(commandBuffer, pMarkerInfo);}
		public static void CmdDebugMarkerEndEXT(IntPtr commandBuffer){ vkCmdDebugMarkerEndEXT(commandBuffer);}
		public static void CmdDebugMarkerInsertEXT(IntPtr commandBuffer, IntPtr pMarkerInfo){ vkCmdDebugMarkerInsertEXT(commandBuffer, pMarkerInfo);}
	}
}