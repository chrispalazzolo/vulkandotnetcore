using System;
using System.Runtime.InteropServices;
using System.Text;

namespace VulkanDotNet
{
	public partial class Vk
	{
		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate IntPtr PFN_vkAllocationFunction(IntPtr pUserData, ulong size, ulong alignment, SystemAllocationScope allocationScope);
		public static PFN_vkAllocationFunction PFNvkAllocationFunction;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate IntPtr PFN_vkReallocationFunction(IntPtr pUserData, IntPtr pOriginal, ulong size, ulong alignment, SystemAllocationScope allocationScope);
		public static PFN_vkReallocationFunction PFNvkReallocationFunction;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkFreeFunction(IntPtr pUserData, IntPtr pMemory);
		public static PFN_vkFreeFunction PFNvkFreeFunction;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkInternalAllocationNotification(IntPtr pUserData, ulong size, InternalAllocationType allocationType, SystemAllocationScope allocationScope);
		public static PFN_vkInternalAllocationNotification PFNvkInternalAllocationNotification;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkInternalFreeNotification(IntPtr pUserData, ulong size, InternalAllocationType allocationType, SystemAllocationScope allocationScope);
		public static PFN_vkInternalFreeNotification PFNvkInternalFreeNotification;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkVoidFunction(void);
		public static PFN_vkVoidFunction PFNvkVoidFunction;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateInstance(IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pInstance);
		public static PFN_vkCreateInstance PFNvkCreateInstance;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyInstance(IntPtr instance, IntPtr pAllocator);
		public static PFN_vkDestroyInstance PFNvkDestroyInstance;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEnumeratePhysicalDevices(IntPtr instance, ref uint pPhysicalDeviceCount, IntPtr pPhysicalDevices);
		public static PFN_vkEnumeratePhysicalDevices PFNvkEnumeratePhysicalDevices;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceFeatures(IntPtr physicalDevice, IntPtr pFeatures);
		public static PFN_vkGetPhysicalDeviceFeatures PFNvkGetPhysicalDeviceFeatures;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceFormatProperties(IntPtr physicalDevice, Format format, IntPtr pFormatProperties);
		public static PFN_vkGetPhysicalDeviceFormatProperties PFNvkGetPhysicalDeviceFormatProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, ImageTiling tiling, uint usage, uint flags, IntPtr pImageFormatProperties);
		public static PFN_vkGetPhysicalDeviceImageFormatProperties PFNvkGetPhysicalDeviceImageFormatProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceProperties(IntPtr physicalDevice, IntPtr pProperties);
		public static PFN_vkGetPhysicalDeviceProperties PFNvkGetPhysicalDeviceProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceQueueFamilyProperties(IntPtr physicalDevice, ref uint pQueueFamilyPropertyCount, IntPtr pQueueFamilyProperties);
		public static PFN_vkGetPhysicalDeviceQueueFamilyProperties PFNvkGetPhysicalDeviceQueueFamilyProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceMemoryProperties(IntPtr physicalDevice, IntPtr pMemoryProperties);
		public static PFN_vkGetPhysicalDeviceMemoryProperties PFNvkGetPhysicalDeviceMemoryProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate PFN_vkVoidFunction PFN_vkGetInstanceProcAddr(IntPtr instance, string pName);
		public static PFN_vkGetInstanceProcAddr PFNvkGetInstanceProcAddr;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate PFN_vkVoidFunction PFN_vkGetDeviceProcAddr(IntPtr device, string pName);
		public static PFN_vkGetDeviceProcAddr PFNvkGetDeviceProcAddr;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDevice(IntPtr physicalDevice, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDevice);
		public static PFN_vkCreateDevice PFNvkCreateDevice;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyDevice(IntPtr device, IntPtr pAllocator);
		public static PFN_vkDestroyDevice PFNvkDestroyDevice;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEnumerateInstanceExtensionProperties(string pLayerName, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkEnumerateInstanceExtensionProperties PFNvkEnumerateInstanceExtensionProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEnumerateDeviceExtensionProperties(IntPtr physicalDevice, string pLayerName, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkEnumerateDeviceExtensionProperties PFNvkEnumerateDeviceExtensionProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEnumerateInstanceLayerProperties(ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkEnumerateInstanceLayerProperties PFNvkEnumerateInstanceLayerProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEnumerateDeviceLayerProperties(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkEnumerateDeviceLayerProperties PFNvkEnumerateDeviceLayerProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetDeviceQueue(IntPtr device, uint queueFamilyIndex, uint queueIndex, IntPtr pQueue);
		public static PFN_vkGetDeviceQueue PFNvkGetDeviceQueue;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkQueueSubmit(IntPtr queue, uint submitCount, IntPtr pSubmits, IntPtr fence);
		public static PFN_vkQueueSubmit PFNvkQueueSubmit;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkQueueWaitIdle(IntPtr queue);
		public static PFN_vkQueueWaitIdle PFNvkQueueWaitIdle;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkDeviceWaitIdle(IntPtr device);
		public static PFN_vkDeviceWaitIdle PFNvkDeviceWaitIdle;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkAllocateMemory(IntPtr device, IntPtr pAllocateInfo, IntPtr pAllocator, IntPtr pMemory);
		public static PFN_vkAllocateMemory PFNvkAllocateMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkFreeMemory(IntPtr device, IntPtr memory, IntPtr pAllocator);
		public static PFN_vkFreeMemory PFNvkFreeMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkMapMemory(IntPtr device, IntPtr memory, ulong offset, ulong size, uint flags, IntPtr ppData);
		public static PFN_vkMapMemory PFNvkMapMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkUnmapMemory(IntPtr device, IntPtr memory);
		public static PFN_vkUnmapMemory PFNvkUnmapMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkFlushMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges);
		public static PFN_vkFlushMappedMemoryRanges PFNvkFlushMappedMemoryRanges;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkInvalidateMappedMemoryRanges(IntPtr device, uint memoryRangeCount, IntPtr pMemoryRanges);
		public static PFN_vkInvalidateMappedMemoryRanges PFNvkInvalidateMappedMemoryRanges;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetDeviceMemoryCommitment(IntPtr device, IntPtr memory, ref ulong pCommittedMemoryInBytes);
		public static PFN_vkGetDeviceMemoryCommitment PFNvkGetDeviceMemoryCommitment;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkBindBufferMemory(IntPtr device, IntPtr buffer, IntPtr memory, ulong memoryOffset);
		public static PFN_vkBindBufferMemory PFNvkBindBufferMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkBindImageMemory(IntPtr device, IntPtr image, IntPtr memory, ulong memoryOffset);
		public static PFN_vkBindImageMemory PFNvkBindImageMemory;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetBufferMemoryRequirements(IntPtr device, IntPtr buffer, IntPtr pMemoryRequirements);
		public static PFN_vkGetBufferMemoryRequirements PFNvkGetBufferMemoryRequirements;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetImageMemoryRequirements(IntPtr device, IntPtr image, IntPtr pMemoryRequirements);
		public static PFN_vkGetImageMemoryRequirements PFNvkGetImageMemoryRequirements;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetImageSparseMemoryRequirements(IntPtr device, IntPtr image, ref uint pSparseMemoryRequirementCount, IntPtr pSparseMemoryRequirements);
		public static PFN_vkGetImageSparseMemoryRequirements PFNvkGetImageSparseMemoryRequirements;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetPhysicalDeviceSparseImageFormatProperties(IntPtr physicalDevice, Format format, ImageType type, SampleCountFlagBits samples, uint usage, ImageTiling tiling, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkGetPhysicalDeviceSparseImageFormatProperties PFNvkGetPhysicalDeviceSparseImageFormatProperties;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkQueueBindSparse(IntPtr queue, uint bindInfoCount, IntPtr pBindInfo, IntPtr fence);
		public static PFN_vkQueueBindSparse PFNvkQueueBindSparse;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateFence(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFence);
		public static PFN_vkCreateFence PFNvkCreateFence;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyFence(IntPtr device, IntPtr fence, IntPtr pAllocator);
		public static PFN_vkDestroyFence PFNvkDestroyFence;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkResetFences(IntPtr device, uint fenceCount, IntPtr pFences);
		public static PFN_vkResetFences PFNvkResetFences;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetFenceStatus(IntPtr device, IntPtr fence);
		public static PFN_vkGetFenceStatus PFNvkGetFenceStatus;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkWaitForFences(IntPtr device, uint fenceCount, IntPtr pFences, uint waitAll, ulong timeout);
		public static PFN_vkWaitForFences PFNvkWaitForFences;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateSemaphore(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSemaphore);
		public static PFN_vkCreateSemaphore PFNvkCreateSemaphore;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroySemaphore(IntPtr device, IntPtr semaphore, IntPtr pAllocator);
		public static PFN_vkDestroySemaphore PFNvkDestroySemaphore;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateEvent(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pEvent);
		public static PFN_vkCreateEvent PFNvkCreateEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyEvent(IntPtr device, IntPtr evnt, IntPtr pAllocator);
		public static PFN_vkDestroyEvent PFNvkDestroyEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetEventStatus(IntPtr device, IntPtr evnt);
		public static PFN_vkGetEventStatus PFNvkGetEventStatus;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkSetEvent(IntPtr device, IntPtr evnt);
		public static PFN_vkSetEvent PFNvkSetEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkResetEvent(IntPtr device, IntPtr evnt);
		public static PFN_vkResetEvent PFNvkResetEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateQueryPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pQueryPool);
		public static PFN_vkCreateQueryPool PFNvkCreateQueryPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyQueryPool(IntPtr device, IntPtr queryPool, IntPtr pAllocator);
		public static PFN_vkDestroyQueryPool PFNvkDestroyQueryPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetQueryPoolResults(IntPtr device, IntPtr queryPool, uint firstQuery, uint queryCount, ulong dataSize, IntPtr pData, ulong stride, uint flags);
		public static PFN_vkGetQueryPoolResults PFNvkGetQueryPoolResults;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateBuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pBuffer);
		public static PFN_vkCreateBuffer PFNvkCreateBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyBuffer(IntPtr device, IntPtr buffer, IntPtr pAllocator);
		public static PFN_vkDestroyBuffer PFNvkDestroyBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateBufferView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView);
		public static PFN_vkCreateBufferView PFNvkCreateBufferView;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyBufferView(IntPtr device, IntPtr bufferView, IntPtr pAllocator);
		public static PFN_vkDestroyBufferView PFNvkDestroyBufferView;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateImage(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pImage);
		public static PFN_vkCreateImage PFNvkCreateImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyImage(IntPtr device, IntPtr image, IntPtr pAllocator);
		public static PFN_vkDestroyImage PFNvkDestroyImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetImageSubresourceLayout(IntPtr device, IntPtr image, IntPtr pSubresource, IntPtr pLayout);
		public static PFN_vkGetImageSubresourceLayout PFNvkGetImageSubresourceLayout;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateImageView(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pView);
		public static PFN_vkCreateImageView PFNvkCreateImageView;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyImageView(IntPtr device, IntPtr imageView, IntPtr pAllocator);
		public static PFN_vkDestroyImageView PFNvkDestroyImageView;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateShaderModule(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pShaderModule);
		public static PFN_vkCreateShaderModule PFNvkCreateShaderModule;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyShaderModule(IntPtr device, IntPtr shaderModule, IntPtr pAllocator);
		public static PFN_vkDestroyShaderModule PFNvkDestroyShaderModule;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreatePipelineCache(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineCache);
		public static PFN_vkCreatePipelineCache PFNvkCreatePipelineCache;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyPipelineCache(IntPtr device, IntPtr pipelineCache, IntPtr pAllocator);
		public static PFN_vkDestroyPipelineCache PFNvkDestroyPipelineCache;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPipelineCacheData(IntPtr device, IntPtr pipelineCache, ref ulong pDataSize, IntPtr pData);
		public static PFN_vkGetPipelineCacheData PFNvkGetPipelineCacheData;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkMergePipelineCaches(IntPtr device, IntPtr dstCache, uint srcCacheCount, IntPtr pSrcCaches);
		public static PFN_vkMergePipelineCaches PFNvkMergePipelineCaches;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateGraphicsPipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines);
		public static PFN_vkCreateGraphicsPipelines PFNvkCreateGraphicsPipelines;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateComputePipelines(IntPtr device, IntPtr pipelineCache, uint createInfoCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pPipelines);
		public static PFN_vkCreateComputePipelines PFNvkCreateComputePipelines;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyPipeline(IntPtr device, IntPtr pipeline, IntPtr pAllocator);
		public static PFN_vkDestroyPipeline PFNvkDestroyPipeline;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreatePipelineLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pPipelineLayout);
		public static PFN_vkCreatePipelineLayout PFNvkCreatePipelineLayout;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyPipelineLayout(IntPtr device, IntPtr pipelineLayout, IntPtr pAllocator);
		public static PFN_vkDestroyPipelineLayout PFNvkDestroyPipelineLayout;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateSampler(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSampler);
		public static PFN_vkCreateSampler PFNvkCreateSampler;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroySampler(IntPtr device, IntPtr sampler, IntPtr pAllocator);
		public static PFN_vkDestroySampler PFNvkDestroySampler;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDescriptorSetLayout(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSetLayout);
		public static PFN_vkCreateDescriptorSetLayout PFNvkCreateDescriptorSetLayout;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyDescriptorSetLayout(IntPtr device, IntPtr descriptorSetLayout, IntPtr pAllocator);
		public static PFN_vkDestroyDescriptorSetLayout PFNvkDestroyDescriptorSetLayout;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDescriptorPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pDescriptorPool);
		public static PFN_vkCreateDescriptorPool PFNvkCreateDescriptorPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyDescriptorPool(IntPtr device, IntPtr descriptorPool, IntPtr pAllocator);
		public static PFN_vkDestroyDescriptorPool PFNvkDestroyDescriptorPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkResetDescriptorPool(IntPtr device, IntPtr descriptorPool, uint flags);
		public static PFN_vkResetDescriptorPool PFNvkResetDescriptorPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkAllocateDescriptorSets(IntPtr device, IntPtr pAllocateInfo, IntPtr pDescriptorSets);
		public static PFN_vkAllocateDescriptorSets PFNvkAllocateDescriptorSets;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkFreeDescriptorSets(IntPtr device, IntPtr descriptorPool, uint descriptorSetCount, IntPtr pDescriptorSets);
		public static PFN_vkFreeDescriptorSets PFNvkFreeDescriptorSets;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkUpdateDescriptorSets(IntPtr device, uint descriptorWriteCount, IntPtr pDescriptorWrites, uint descriptorCopyCount, IntPtr pDescriptorCopies);
		public static PFN_vkUpdateDescriptorSets PFNvkUpdateDescriptorSets;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateFramebuffer(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pFramebuffer);
		public static PFN_vkCreateFramebuffer PFNvkCreateFramebuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyFramebuffer(IntPtr device, IntPtr framebuffer, IntPtr pAllocator);
		public static PFN_vkDestroyFramebuffer PFNvkDestroyFramebuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateRenderPass(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pRenderPass);
		public static PFN_vkCreateRenderPass PFNvkCreateRenderPass;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyRenderPass(IntPtr device, IntPtr renderPass, IntPtr pAllocator);
		public static PFN_vkDestroyRenderPass PFNvkDestroyRenderPass;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkGetRenderAreaGranularity(IntPtr device, IntPtr renderPass, IntPtr pGranularity);
		public static PFN_vkGetRenderAreaGranularity PFNvkGetRenderAreaGranularity;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateCommandPool(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCommandPool);
		public static PFN_vkCreateCommandPool PFNvkCreateCommandPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyCommandPool(IntPtr device, IntPtr commandPool, IntPtr pAllocator);
		public static PFN_vkDestroyCommandPool PFNvkDestroyCommandPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkResetCommandPool(IntPtr device, IntPtr commandPool, uint flags);
		public static PFN_vkResetCommandPool PFNvkResetCommandPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkAllocateCommandBuffers(IntPtr device, IntPtr pAllocateInfo, IntPtr pCommandBuffers);
		public static PFN_vkAllocateCommandBuffers PFNvkAllocateCommandBuffers;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkFreeCommandBuffers(IntPtr device, IntPtr commandPool, uint commandBufferCount, IntPtr pCommandBuffers);
		public static PFN_vkFreeCommandBuffers PFNvkFreeCommandBuffers;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkBeginCommandBuffer(IntPtr commandBuffer, IntPtr pBeginInfo);
		public static PFN_vkBeginCommandBuffer PFNvkBeginCommandBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkEndCommandBuffer(IntPtr commandBuffer);
		public static PFN_vkEndCommandBuffer PFNvkEndCommandBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkResetCommandBuffer(IntPtr commandBuffer, uint flags);
		public static PFN_vkResetCommandBuffer PFNvkResetCommandBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBindPipeline(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr pipeline);
		public static PFN_vkCmdBindPipeline PFNvkCmdBindPipeline;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetViewport(IntPtr commandBuffer, uint firstViewport, uint viewportCount, IntPtr pViewports);
		public static PFN_vkCmdSetViewport PFNvkCmdSetViewport;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetScissor(IntPtr commandBuffer, uint firstScissor, uint scissorCount, IntPtr pScissors);
		public static PFN_vkCmdSetScissor PFNvkCmdSetScissor;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetLineWidth(IntPtr commandBuffer, float lineWidth);
		public static PFN_vkCmdSetLineWidth PFNvkCmdSetLineWidth;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetDepthBias(IntPtr commandBuffer, float depthBiasConstantFactor, float depthBiasClamp, float depthBiasSlopeFactor);
		public static PFN_vkCmdSetDepthBias PFNvkCmdSetDepthBias;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetBlendConstants(IntPtr commandBuffer, float[] blendConstants);
		public static PFN_vkCmdSetBlendConstants PFNvkCmdSetBlendConstants;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetDepthBounds(IntPtr commandBuffer, float minDepthBounds, float maxDepthBounds);
		public static PFN_vkCmdSetDepthBounds PFNvkCmdSetDepthBounds;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetStencilCompareMask(IntPtr commandBuffer, uint faceMask, uint compareMask);
		public static PFN_vkCmdSetStencilCompareMask PFNvkCmdSetStencilCompareMask;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetStencilWriteMask(IntPtr commandBuffer, uint faceMask, uint writeMask);
		public static PFN_vkCmdSetStencilWriteMask PFNvkCmdSetStencilWriteMask;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetStencilReference(IntPtr commandBuffer, uint faceMask, uint reference);
		public static PFN_vkCmdSetStencilReference PFNvkCmdSetStencilReference;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBindDescriptorSets(IntPtr commandBuffer, PipelineBindPoint pipelineBindPoint, IntPtr layout, uint firstSet, uint descriptorSetCount, IntPtr pDescriptorSets, uint dynamicOffsetCount, ref uint pDynamicOffsets);
		public static PFN_vkCmdBindDescriptorSets PFNvkCmdBindDescriptorSets;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBindIndexBuffer(IntPtr commandBuffer, IntPtr buffer, ulong offset, IndexType indexType);
		public static PFN_vkCmdBindIndexBuffer PFNvkCmdBindIndexBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBindVertexBuffers(IntPtr commandBuffer, uint firstBinding, uint bindingCount, IntPtr pBuffers, ref ulong pOffsets);
		public static PFN_vkCmdBindVertexBuffers PFNvkCmdBindVertexBuffers;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDraw(IntPtr commandBuffer, uint vertexCount, uint instanceCount, uint firstVertex, uint firstInstance);
		public static PFN_vkCmdDraw PFNvkCmdDraw;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDrawIndexed(IntPtr commandBuffer, uint indexCount, uint instanceCount, uint firstIndex, int vertexOffset, uint firstInstance);
		public static PFN_vkCmdDrawIndexed PFNvkCmdDrawIndexed;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDrawIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride);
		public static PFN_vkCmdDrawIndirect PFNvkCmdDrawIndirect;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDrawIndexedIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset, uint drawCount, uint stride);
		public static PFN_vkCmdDrawIndexedIndirect PFNvkCmdDrawIndexedIndirect;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDispatch(IntPtr commandBuffer, uint x, uint y, uint z);
		public static PFN_vkCmdDispatch PFNvkCmdDispatch;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDispatchIndirect(IntPtr commandBuffer, IntPtr buffer, ulong offset);
		public static PFN_vkCmdDispatchIndirect PFNvkCmdDispatchIndirect;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdCopyBuffer(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstBuffer, uint regionCount, IntPtr pRegions);
		public static PFN_vkCmdCopyBuffer PFNvkCmdCopyBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdCopyImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);
		public static PFN_vkCmdCopyImage PFNvkCmdCopyImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBlitImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions, Filter filter);
		public static PFN_vkCmdBlitImage PFNvkCmdBlitImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdCopyBufferToImage(IntPtr commandBuffer, IntPtr srcBuffer, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);
		public static PFN_vkCmdCopyBufferToImage PFNvkCmdCopyBufferToImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdCopyImageToBuffer(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstBuffer, uint regionCount, IntPtr pRegions);
		public static PFN_vkCmdCopyImageToBuffer PFNvkCmdCopyImageToBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdUpdateBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong dataSize, IntPtr pData);
		public static PFN_vkCmdUpdateBuffer PFNvkCmdUpdateBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdFillBuffer(IntPtr commandBuffer, IntPtr dstBuffer, ulong dstOffset, ulong size, uint data);
		public static PFN_vkCmdFillBuffer PFNvkCmdFillBuffer;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdClearColorImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pColor, uint rangeCount, IntPtr pRanges);
		public static PFN_vkCmdClearColorImage PFNvkCmdClearColorImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdClearDepthStencilImage(IntPtr commandBuffer, IntPtr image, ImageLayout imageLayout, IntPtr pDepthStencil, uint rangeCount, IntPtr pRanges);
		public static PFN_vkCmdClearDepthStencilImage PFNvkCmdClearDepthStencilImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdClearAttachments(IntPtr commandBuffer, uint attachmentCount, IntPtr pAttachments, uint rectCount, IntPtr pRects);
		public static PFN_vkCmdClearAttachments PFNvkCmdClearAttachments;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdResolveImage(IntPtr commandBuffer, IntPtr srcImage, ImageLayout srcImageLayout, IntPtr dstImage, ImageLayout dstImageLayout, uint regionCount, IntPtr pRegions);
		public static PFN_vkCmdResolveImage PFNvkCmdResolveImage;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdSetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask);
		public static PFN_vkCmdSetEvent PFNvkCmdSetEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdResetEvent(IntPtr commandBuffer, IntPtr evnt, uint stageMask);
		public static PFN_vkCmdResetEvent PFNvkCmdResetEvent;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdWaitEvents(IntPtr commandBuffer, uint eventCount, IntPtr pEvents, uint srcStageMask, uint dstStageMask, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers);
		public static PFN_vkCmdWaitEvents PFNvkCmdWaitEvents;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdPipelineBarrier(IntPtr commandBuffer, uint srcStageMask, uint dstStageMask, uint dependencyFlags, uint memoryBarrierCount, IntPtr pMemoryBarriers, uint bufferMemoryBarrierCount, IntPtr pBufferMemoryBarriers, uint imageMemoryBarrierCount, IntPtr pImageMemoryBarriers);
		public static PFN_vkCmdPipelineBarrier PFNvkCmdPipelineBarrier;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBeginQuery(IntPtr commandBuffer, IntPtr queryPool, uint query, uint flags);
		public static PFN_vkCmdBeginQuery PFNvkCmdBeginQuery;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdEndQuery(IntPtr commandBuffer, IntPtr queryPool, uint query);
		public static PFN_vkCmdEndQuery PFNvkCmdEndQuery;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdResetQueryPool(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount);
		public static PFN_vkCmdResetQueryPool PFNvkCmdResetQueryPool;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdWriteTimestamp(IntPtr commandBuffer, PipelineStageFlagBits pipelineStage, IntPtr queryPool, uint query);
		public static PFN_vkCmdWriteTimestamp PFNvkCmdWriteTimestamp;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdCopyQueryPoolResults(IntPtr commandBuffer, IntPtr queryPool, uint firstQuery, uint queryCount, IntPtr dstBuffer, ulong dstOffset, ulong stride, uint flags);
		public static PFN_vkCmdCopyQueryPoolResults PFNvkCmdCopyQueryPoolResults;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdPushConstants(IntPtr commandBuffer, IntPtr layout, uint stageFlags, uint offset, uint size, IntPtr pValues);
		public static PFN_vkCmdPushConstants PFNvkCmdPushConstants;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdBeginRenderPass(IntPtr commandBuffer, IntPtr pRenderPassBegin, SubpassContents contents);
		public static PFN_vkCmdBeginRenderPass PFNvkCmdBeginRenderPass;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdNextSubpass(IntPtr commandBuffer, SubpassContents contents);
		public static PFN_vkCmdNextSubpass PFNvkCmdNextSubpass;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdEndRenderPass(IntPtr commandBuffer);
		public static PFN_vkCmdEndRenderPass PFNvkCmdEndRenderPass;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdExecuteCommands(IntPtr commandBuffer, uint commandBufferCount, IntPtr pCommandBuffers);
		public static PFN_vkCmdExecuteCommands PFNvkCmdExecuteCommands;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroySurfaceKHR(IntPtr instance, IntPtr surface, IntPtr pAllocator);
		public static PFN_vkDestroySurfaceKHR PFNvkDestroySurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceSurfaceSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr surface, ref uint pSupported);
		public static PFN_vkGetPhysicalDeviceSurfaceSupportKHR PFNvkGetPhysicalDeviceSurfaceSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(IntPtr physicalDevice, IntPtr surface, IntPtr pSurfaceCapabilities);
		public static PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR PFNvkGetPhysicalDeviceSurfaceCapabilitiesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceSurfaceFormatsKHR(IntPtr physicalDevice, IntPtr surface, ref uint pSurfaceFormatCount, IntPtr pSurfaceFormats);
		public static PFN_vkGetPhysicalDeviceSurfaceFormatsKHR PFNvkGetPhysicalDeviceSurfaceFormatsKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceSurfacePresentModesKHR(IntPtr physicalDevice, IntPtr surface, ref uint pPresentModeCount, IntPtr pPresentModes);
		public static PFN_vkGetPhysicalDeviceSurfacePresentModesKHR PFNvkGetPhysicalDeviceSurfacePresentModesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateSwapchainKHR(IntPtr device, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSwapchain);
		public static PFN_vkCreateSwapchainKHR PFNvkCreateSwapchainKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroySwapchainKHR(IntPtr device, IntPtr swapchain, IntPtr pAllocator);
		public static PFN_vkDestroySwapchainKHR PFNvkDestroySwapchainKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetSwapchainImagesKHR(IntPtr device, IntPtr swapchain, ref uint pSwapchainImageCount, IntPtr pSwapchainImages);
		public static PFN_vkGetSwapchainImagesKHR PFNvkGetSwapchainImagesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkAcquireNextImageKHR(IntPtr device, IntPtr swapchain, ulong timeout, IntPtr semaphore, IntPtr fence, ref uint pImageIndex);
		public static PFN_vkAcquireNextImageKHR PFNvkAcquireNextImageKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkQueuePresentKHR(IntPtr queue, IntPtr pPresentInfo);
		public static PFN_vkQueuePresentKHR PFNvkQueuePresentKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceDisplayPropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkGetPhysicalDeviceDisplayPropertiesKHR PFNvkGetPhysicalDeviceDisplayPropertiesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetPhysicalDeviceDisplayPlanePropertiesKHR(IntPtr physicalDevice, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkGetPhysicalDeviceDisplayPlanePropertiesKHR PFNvkGetPhysicalDeviceDisplayPlanePropertiesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetDisplayPlaneSupportedDisplaysKHR(IntPtr physicalDevice, uint planeIndex, ref uint pDisplayCount, IntPtr pDisplays);
		public static PFN_vkGetDisplayPlaneSupportedDisplaysKHR PFNvkGetDisplayPlaneSupportedDisplaysKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetDisplayModePropertiesKHR(IntPtr physicalDevice, IntPtr display, ref uint pPropertyCount, IntPtr pProperties);
		public static PFN_vkGetDisplayModePropertiesKHR PFNvkGetDisplayModePropertiesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDisplayModeKHR(IntPtr physicalDevice, IntPtr display, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pMode);
		public static PFN_vkCreateDisplayModeKHR PFNvkCreateDisplayModeKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkGetDisplayPlaneCapabilitiesKHR(IntPtr physicalDevice, IntPtr mode, uint planeIndex, IntPtr pCapabilities);
		public static PFN_vkGetDisplayPlaneCapabilitiesKHR PFNvkGetDisplayPlaneCapabilitiesKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDisplayPlaneSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateDisplayPlaneSurfaceKHR PFNvkCreateDisplayPlaneSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateSharedSwapchainsKHR(IntPtr device, uint swapchainCount, IntPtr pCreateInfos, IntPtr pAllocator, IntPtr pSwapchains);
		public static PFN_vkCreateSharedSwapchainsKHR PFNvkCreateSharedSwapchainsKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateXlibSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateXlibSurfaceKHR PFNvkCreateXlibSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkGetPhysicalDeviceXlibPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr dpy, UInt64 visualID);
		public static PFN_vkGetPhysicalDeviceXlibPresentationSupportKHR PFNvkGetPhysicalDeviceXlibPresentationSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateXcbSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateXcbSurfaceKHR PFNvkCreateXcbSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkGetPhysicalDeviceXcbPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection, UInt32 visual_id);
		public static PFN_vkGetPhysicalDeviceXcbPresentationSupportKHR PFNvkGetPhysicalDeviceXcbPresentationSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateWaylandSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateWaylandSurfaceKHR PFNvkCreateWaylandSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkGetPhysicalDeviceWaylandPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr wl_display);
		public static PFN_vkGetPhysicalDeviceWaylandPresentationSupportKHR PFNvkGetPhysicalDeviceWaylandPresentationSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateMirSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateMirSurfaceKHR PFNvkCreateMirSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkGetPhysicalDeviceMirPresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex, IntPtr connection);
		public static PFN_vkGetPhysicalDeviceMirPresentationSupportKHR PFNvkGetPhysicalDeviceMirPresentationSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateAndroidSurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateAndroidSurfaceKHR PFNvkCreateAndroidSurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateWin32SurfaceKHR(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pSurface);
		public static PFN_vkCreateWin32SurfaceKHR PFNvkCreateWin32SurfaceKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkGetPhysicalDeviceWin32PresentationSupportKHR(IntPtr physicalDevice, uint queueFamilyIndex);
		public static PFN_vkGetPhysicalDeviceWin32PresentationSupportKHR PFNvkGetPhysicalDeviceWin32PresentationSupportKHR;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate uint PFN_vkDebugReportCallbackEXT(uint flags, DebugReportObjectTypeEXT objectType, ulong obj, ulong location, int messageCode, string pLayerPrefix, string pMessage, IntPtr pUserData);
		public static PFN_vkDebugReportCallbackEXT PFNvkDebugReportCallbackEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkCreateDebugReportCallbackEXT(IntPtr instance, IntPtr pCreateInfo, IntPtr pAllocator, IntPtr pCallback);
		public static PFN_vkCreateDebugReportCallbackEXT PFNvkCreateDebugReportCallbackEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDestroyDebugReportCallbackEXT(IntPtr instance, IntPtr callback, IntPtr pAllocator);
		public static PFN_vkDestroyDebugReportCallbackEXT PFNvkDestroyDebugReportCallbackEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkDebugReportMessageEXT(IntPtr instance, uint flags, DebugReportObjectTypeEXT objectType, ulong obj, ulong location, int messageCode, string pLayerPrefix, string pMessage);
		public static PFN_vkDebugReportMessageEXT PFNvkDebugReportMessageEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkDebugMarkerSetObjectTagEXT(IntPtr device, IntPtr pTagInfo);
		public static PFN_vkDebugMarkerSetObjectTagEXT PFNvkDebugMarkerSetObjectTagEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate Result PFN_vkDebugMarkerSetObjectNameEXT(IntPtr device, IntPtr pNameInfo);
		public static PFN_vkDebugMarkerSetObjectNameEXT PFNvkDebugMarkerSetObjectNameEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDebugMarkerBeginEXT(IntPtr commandBuffer, IntPtr pMarkerInfo);
		public static PFN_vkCmdDebugMarkerBeginEXT PFNvkCmdDebugMarkerBeginEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDebugMarkerEndEXT(IntPtr commandBuffer);
		public static PFN_vkCmdDebugMarkerEndEXT PFNvkCmdDebugMarkerEndEXT;

		[UnmanagedFunctionPointer(CallingConvention.Winapi)]
		public delegate void PFN_vkCmdDebugMarkerInsertEXT(IntPtr commandBuffer, IntPtr pMarkerInfo);
		public static PFN_vkCmdDebugMarkerInsertEXT PFNvkCmdDebugMarkerInsertEXT;

	}
}